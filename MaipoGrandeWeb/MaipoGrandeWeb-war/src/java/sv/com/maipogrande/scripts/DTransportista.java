/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.scripts;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.HorizontalBarChartModel;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import sv.com.maipogrande.entities.TEmpresa;
import sv.com.maipogrande.entities.TUsuario;
import sv.com.maipogrande.entities.TOfertaTransporte;
import sv.com.maipogrande.entities.TPedido;
import sv.com.maipogrande.entities.VViajesmeses;
import sv.com.maipogrande.model.VViajesmesesFacade;
import sv.com.maipogrande.model.TUsuarioFacade;
import sv.com.maipogrande.model.TPedidoFacade;
import sv.com.maipogrande.model.TOfertaTransporteFacade;
import sv.com.maipogrande.model.TEmpresaFacade;
import sv.com.maipogrande.session.Util;

/**
 *
 * @author Juan Hernández
 */
@Named(value = "dTransportista")
@SessionScoped
public class DTransportista implements Serializable {

    //ISTANCIAS FACADE PARA ACCEDER A LAS CLASES EN LA BASE DE DATOS.
    @EJB
    private TUsuarioFacade tUsuarioFacade;
    
    @EJB
    private TOfertaTransporteFacade tOfertaTransporteFacade;
    
    @EJB
    private TPedidoFacade tPedidoFacade;
    
    @EJB
    private TEmpresaFacade tEmpresaFacade;
    
//    private VViajesmesesFacade vViajesmesesFacade;

    //INSTANCIAS DE CLASES NECESARIAS PARA EL GRAFICO.
    private TUsuario tUsuario;
    
    private TOfertaTransporte tOfertaTransporte;
    
    private TPedido tPedido;
    
    private TEmpresa tEmpresa;
    
//    private VViajesmeses vViajesmeses;
    
    //INSTANCIAS DE GRAFICO DE LINEAS.
    private BarChartModel barModel;

    private LineChartModel lineModel2;

    private LineChartModel zoomModel;
    
    //METODO INIT PARA INICIALIZAR EL GRAFICO.
//    @PostConstruct
//    public void init() {
//        createLineModels();
//    }
    
    //CONSTRUCTOR
    public DTransportista(){
        tPedido = new TPedido();
        tOfertaTransporte =  new TOfertaTransporte();
        tEmpresa = new TEmpresa();
//        vViajesmeses = new VViajesmeses();
    }

    //METODO GETTER AND SETTER.
    public LineChartModel getLineModel2() {
        return lineModel2;
    }

    public LineChartModel getZoomModel() {
        return zoomModel;
    }
    
    public TPedido getPedido() {
        return tPedido;
    }

    public void setPedido(TPedido pedido) {
        this.tPedido = tPedido;
    }
    
    public TOfertaTransporte getOfertaTransporte(){
        return tOfertaTransporte;
    }
    
    public void setOfertaTransporte(TOfertaTransporte tOfertaTransporte){
        this.tOfertaTransporte = tOfertaTransporte;
    }
    
    public TEmpresa getEmpresa(){
        return tEmpresa;
    }
    
    public void setEmpresa(TEmpresa tEmpresa){
        this.tEmpresa = tEmpresa;
    }
    
//    public VViajesmeses getViajesMeses(){
//        return vViajesmeses;
//    }
//    
//    public void setViajeMeses(VViajesmeses vViajesmeses){
//        this.vViajesmeses = vViajesmeses;
//    }

//    //METODO QUE CREA EL GRAFICO DE LINEAS.
//    private void createLineModels() {
//        lineModel2 = initCategoryModel();
//        lineModel2.setTitle("Viajes Completos por Mes");
//        lineModel2.setLegendPosition("e");
//        lineModel2.setShowPointLabels(true);
//        lineModel2.getAxes().put(AxisType.X, new CategoryAxis(""));
//        Axis yAxis = lineModel2.getAxis(AxisType.Y);
//        yAxis.setLabel("Viajes");
//        yAxis.setMin(0);
//        yAxis.setTickInterval("5");
//
//        zoomModel = initLinearModel();
//        zoomModel.setTitle("Zoom");
//        zoomModel.setZoom(true);
//        zoomModel.setLegendPosition("e");
//        yAxis = zoomModel.getAxis(AxisType.Y);
//        yAxis.setMin(0);
//        yAxis.setMax(10);
//    }

//    //METODO QUE RELLENA EL GRAFICO DE LINEAS.
//    private LineChartModel initCategoryModel() {
//        LineChartModel model = new LineChartModel();
//
//        ChartSeries viaje = new ChartSeries();
//        viaje.setLabel("Viajes");
//
//        viaje.set("Enero", findViajesMeses("01"));
//        viaje.set("Febrero", findViajesMeses("02"));
//        viaje.set("Marzo", findViajesMeses("03"));
//        viaje.set("Abril", findViajesMeses("04"));
//        viaje.set("Mayo", findViajesMeses("05"));
//        viaje.set("Junio", findViajesMeses("06"));
//        viaje.set("Julio", findViajesMeses("07"));
//        viaje.set("Agosto", findViajesMeses("08")); 
//        viaje.set("Septiembre", findViajesMeses("09"));
//        viaje.set("Octubre", findViajesMeses("10"));
//        viaje.set("Noviembre", findViajesMeses("11"));
//        viaje.set("Diciembre", findViajesMeses("12"));
//
//        model.addSeries(viaje);
//        return model;
//    }

    //METODO QUE INICIALIZA ZOOMMODEL.
    private LineChartModel initLinearModel() {
        LineChartModel model = new LineChartModel();

        LineChartSeries series1 = new LineChartSeries();
        series1.setLabel("");

        series1.set(1, 2);
        series1.set(2, 1);
        series1.set(3, 3);
        series1.set(4, 6);
        series1.set(5, 8);

        model.addSeries(series1);

        return model;
    }

    //METODO QUE OBTIENE EL USUARIO LOGEADO.
    TUsuario obtenerUsuario() {
        tUsuario = (TUsuario) Util.getSession().getAttribute("usuario");
        tUsuario = tUsuarioFacade.find(tUsuario.getIdusuario());
        return tUsuario;
    }
    
//    //METODO QUE OBTIENE LA CANTIDAD DE GANANCIAS POR MES Y USUARIO LOGEADO.
//    public int findViajesMeses(String mes) {
//        int Mes = Integer.parseInt(mes);
//        int IdUsu = obtenerUsuario().getIdrepresentante().getIdrepresentante().intValue();
//
//        return tPedidoFacade.findViajesMeses(IdUsu, Mes);
//    }
    
}

