/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.scripts;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import java.io.Serializable;
import javax.ejb.EJB;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.ChartSeries;
import sv.com.maipogrande.entities.TLoteOfrecido;
import sv.com.maipogrande.entities.TPedido;
import sv.com.maipogrande.entities.TUsuario;
import sv.com.maipogrande.model.TLoteOfrecidoFacade;
import sv.com.maipogrande.model.TPedidoFacade;
import sv.com.maipogrande.model.TUsuarioFacade;
import sv.com.maipogrande.session.Util;

/**
 *
 * @author Juan Román
 */
@Named
@RequestScoped
public class DProductor implements Serializable {

    /*-------------Instancias  facade para acceder a las clases en la Base de datos------------*/
    
    @EJB
    private TLoteOfrecidoFacade tLoteOfrecidoFacade;

    @EJB
    private TPedidoFacade tPedidoFacade;

    @EJB
    private TUsuarioFacade tUsuarioFacade;

    /*-----------Instancias  de clases necesarias para el grafico------------------*/
    private TLoteOfrecido loteOfrecido;

    private TPedido pedido;

    private TUsuario usuario;

    /*-------------Instancias  de gráfico de líneas-------------*/
    
    private LineChartModel GraficosLineasProductor;

    private LineChartModel zoomModel;

    /*-----------Metodo Init para inicializar grafico ---------*/
    
    
    @PostConstruct
    public void init() {
        createLineModels();
    }

    /*-------Constructor--------*/
    
    public DProductor() {
        pedido = new TPedido();
        loteOfrecido = new TLoteOfrecido();
    }

     /*------------ Método Get y Set ----------*/
    public LineChartModel getLineModel2() {
        return GraficosLineasProductor;
    }

    public LineChartModel getZoomModel() {
        return zoomModel;
    }
    public TLoteOfrecido getLoteOfrecido() {
        return loteOfrecido;
    }

    public void setLoteOfrecido(TLoteOfrecido loteOfrecido) {
        this.loteOfrecido = loteOfrecido;
    }

    public TPedido getPedido() {
        return pedido;
    }

    public void setPedido(TPedido pedido) {
        this.pedido = pedido;
    }

    /*Método que crea el grafico de líneas - Juan Román*/
    
    private void createLineModels() {
        GraficosLineasProductor = initCategoryModel();
        GraficosLineasProductor.setTitle("Ventas Completadas Por Mes");
        GraficosLineasProductor.setLegendPosition("e");
        GraficosLineasProductor.setShowPointLabels(true);
        GraficosLineasProductor.getAxes().put(AxisType.X, new CategoryAxis(""));
        Axis yAxis = GraficosLineasProductor.getAxis(AxisType.Y);
        yAxis.setLabel("Ventas");
        yAxis.setMin(0);
        yAxis.setTickInterval("5");
        
        zoomModel = initLinearModel();
        zoomModel.setTitle("Zoom");
        zoomModel.setZoom(true);
        zoomModel.setLegendPosition("e");
        yAxis = zoomModel.getAxis(AxisType.Y);
        yAxis.setMin(0);
        yAxis.setMax(10);
    }

    /*Método que rellena el grafico de líneas - Juan Román*/
    
    private LineChartModel initCategoryModel() {
        LineChartModel model = new LineChartModel();

        ChartSeries ventas = new ChartSeries();
        ventas.setLabel("Ofertas");

        ventas.set("Enero", VentasMes("01"));
        ventas.set("Febrero", VentasMes("02"));
        ventas.set("Marzo", VentasMes("03"));
        ventas.set("Abril", VentasMes("04"));
        ventas.set("Mayo", VentasMes("05"));
        ventas.set("Junio", VentasMes("06"));
        ventas.set("Julio", VentasMes("07"));
        ventas.set("Agosto", VentasMes("08"));
        /*en String por que int no aguanta 08 - 09*/
        ventas.set("Septiembre", VentasMes("09"));
        ventas.set("Octubre", VentasMes("10"));
        ventas.set("Noviembre", VentasMes("11"));
        ventas.set("Diciembre", VentasMes("12"));

        model.addSeries(ventas);
        return model;
    }

    /*Método que inicializa zoomModel - Juan Román*/
    
    private LineChartModel initLinearModel() {
        LineChartModel model = new LineChartModel();

        LineChartSeries series1 = new LineChartSeries();
        series1.setLabel("");

        series1.set(1, 2);
        series1.set(2, 1);
        series1.set(3, 3);
        series1.set(4, 6);
        series1.set(5, 8);

        model.addSeries(series1);

        return model;
    }

    /*Método que obtiene el usuario logeado - Juan Román*/
    TUsuario obtenerUsuario() {
        usuario = (TUsuario) Util.getSession().getAttribute("usuario");
        usuario = tUsuarioFacade.find(usuario.getIdusuario());
        return usuario;
    }

    /*Método que obtiene la cantidad de ofertas realizadas a pedidos por mes y usuario logeado - Juan Román*/
    
    public int VentasMes(String mes) {
        int Mes = Integer.parseInt(mes);
        int IdUsu = obtenerUsuario().getIdrepresentante().getIdrepresentante().intValue();

        return tPedidoFacade.findCountPedidos(IdUsu, Mes);
    }

}
