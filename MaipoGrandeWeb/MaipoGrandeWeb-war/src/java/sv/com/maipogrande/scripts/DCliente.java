package sv.com.maipogrande.scripts;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.HorizontalBarChartModel;
import sv.com.maipogrande.entities.TUsuario;
import sv.com.maipogrande.model.TProductoPedidoFacade;
import sv.com.maipogrande.model.TUsuarioFacade;
import sv.com.maipogrande.session.Util;

/**
 * Bean con métodos para los gráficos del dashboard de clientes
 *
 * @author Cristián Esquivel
 */
@Named(value = "dCliente")
@SessionScoped
public class DCliente implements Serializable {

    //================================================================================
    // Instancias de facades para acceder a las clases de la base de datos
    //================================================================================  
    @EJB
    private TProductoPedidoFacade tProductoPedidoFacade;

    @EJB
    private TUsuarioFacade tUsuarioFacade;

    //================================================================================
    // Instancia del usuario iniciado sesión
    //================================================================================  
    TUsuario usuario;

    //================================================================================
    // Instancia del gráfico horizontal con su getter
    //================================================================================  
    private HorizontalBarChartModel barChartProductos;

    public HorizontalBarChartModel getBarChartProductos() {
        return barChartProductos;
    }

    //================================================================================
    // Constructor
    //================================================================================  
    public DCliente() {
    }

    //================================================================================
    // Método Init para inicializar los gráficos
    //================================================================================  
    @PostConstruct
    public void init() {
        createLineModels();
    }

    //================================================================================
    // Método que rellena el gráfico horizontal con datos
    //================================================================================  
    private void createLineModels() {
        HorizontalBarChartModel model = new HorizontalBarChartModel();

        ChartSeries producto = new ChartSeries();
        producto.setLabel("Producto");

        List<Object[]> o = tProductoPedidoFacade.findProductosMasPedidosMeses(3);// Llama al facade para extraer los productos más pedidos en los últimos X meses
        Collections.reverse(o);
        for (Object[] d : o) {
            producto.set(d[0].toString() + " " + d[1].toString(), Integer.parseInt(d[2].toString())); // Extrae los datos de la lista de objetos el formato sería Nombre producto + Tipo Producto - Cantidad de veces pedidas
        }
        model.addSeries(producto);
        barChartProductos = model;
        barChartProductos.setTitle("Productos Más Solicitados los Últimos 3 Meses");// Título del gráfico
        barChartProductos.setShowPointLabels(true); // Muestra el puntero de la cantidad, un número que se muestra en cada barra
        barChartProductos.setLegendPosition("e"); // Posición de la leyenda "Producto", en este caso al Este del gráfico
        barChartProductos.setAnimate(true); // Animación del gráfico
        Axis xAxis = barChartProductos.getAxis(AxisType.X);
        xAxis.setLabel("Cantidad");
        xAxis.setMin(0);
        xAxis.setTickInterval("10"); // Intervalos de cantidad

        Axis yAxis = barChartProductos.getAxis(AxisType.Y);
        yAxis.setLabel("Producto");
    }

    //================================================================================
    // Método que obtiene el usuario iniciado sesión
    //================================================================================  
    public TUsuario obtenerUsuario() {
        usuario = (TUsuario) Util.getSession().getAttribute("usuario");
        usuario = tUsuarioFacade.find(usuario.getIdusuario());
        return usuario;
    }
}
