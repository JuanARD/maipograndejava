package sv.com.maipogrande.scripts;

/**
 *
 * @author Leb
 */
public class Funciones {

    /**
     * @param clase Este parámetro es la ruta de memoria de la clase. Ej: sv.com.maipogrande.entities.TEmpresa[ idempresa=2 ]
     * @return int
     */
    public static int obtenerIdDeClase(String clase) {
        String texto;
        int id;
        try {
            texto = clase.replaceAll(".*=+", " "); //saca el id numérico del texto (resta limpiarlo todavía)
            texto = texto.replaceAll("\\D", " ").trim(); //id limpio, sin espacios en blanco ni corchete final
            id = Integer.parseInt(texto);
        } catch (Exception e) {
            return 0;
        }
        return id;
    }
}
