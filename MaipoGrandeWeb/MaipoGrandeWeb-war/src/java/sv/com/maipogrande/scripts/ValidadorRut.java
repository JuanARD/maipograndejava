/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.scripts;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author Maxi
 */
@FacesValidator(value = "ValidadorRut")
  public  class ValidadorRut implements Validator {

        @Override
        public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

            String texto = (String) value;

            if (validaRut(texto) == false) {
                FacesMessage fm = new FacesMessage(FacesMessage.FACES_MESSAGES, "Rut Inválido");
                throw new ValidatorException(fm);
            }
        }

        public String dv(String rut) {
            Integer M = 0, S = 1, T = Integer.parseInt(rut);
            for (; T != 0; T = (int) Math.floor(T /= 10)) {
                S = (S + T % 10 * (9 - M++ % 6)) % 11;
            }
            return (S > 0) ? String.valueOf(S - 1) : "k";
        }

        public Boolean validaRut(String rut) {
            Pattern pattern = Pattern.compile("^[0-9]+-[0-9kK]{1}$");
            Matcher matcher = pattern.matcher(rut);
            if (matcher.matches() == false) {
                return false;
            }
            String[] stringRut = rut.split("-");
            return stringRut[1].toLowerCase().equals(dv(stringRut[0]));
        }

    }
