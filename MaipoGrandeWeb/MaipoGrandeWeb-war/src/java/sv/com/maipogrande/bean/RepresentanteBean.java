/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.bean;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import sv.com.maipogrande.entities.TEmpresa;
import sv.com.maipogrande.entities.TRepresentante;
import sv.com.maipogrande.entities.TUsuario;
import sv.com.maipogrande.model.TEmpresaFacade;
import sv.com.maipogrande.model.TRepresentanteFacade;
import sv.com.maipogrande.session.Util;

/**
 *
 * @author Maximiliano González
 */
@Named(value = "representanteBean")
@RequestScoped
public class RepresentanteBean {

    @EJB
    private TEmpresaFacade tEmpresaFacade;

    @EJB
    private TRepresentanteFacade tRepresentanteFacade;
    
    

    private BigDecimal id_representante;
    private String rut_representante;
    private String rol_representante;
    private String nombre;
    private String appaterno;
    private String apmaterno;
    private char sexo;
    private Date fecha_nacimiento;
    private String fono_1;
    private String fono_2;
    private String direccion;
    private char vigente;
    private TEmpresa empresa;
    
    public RepresentanteBean() {
        empresa = new TEmpresa();
    }
    
    public List<TRepresentante> getRepresentantes() {
        return tRepresentanteFacade.findAll();
    }

    public String crear() {
        TRepresentante rep = new TRepresentante();

        rep.setIdrepresentante(id_representante);
        rep.setIdempresa(empresa);
        rep.setRutrepresentante(rut_representante);
        rep.setRolrepresentante(rol_representante);
        rep.setNombre(nombre);
        rep.setAppaterno(appaterno);
        rep.setApmaterno(apmaterno);
        rep.setSexo(sexo);
        rep.setFechanacimiento(fecha_nacimiento);
        rep.setFono1(fono_1);
        rep.setFono2(fono_2);
        rep.setDireccion(direccion);
        rep.setVigente(vigente);
        tRepresentanteFacade.create(rep);

        return "representante";
    }

    public TEmpresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(TEmpresa empresa) {
        this.empresa = empresa;
    }


    public BigDecimal getId_representante() {
        return id_representante;
    }

    public void setId_representante(BigDecimal id_representante) {
        this.id_representante = id_representante;
    }

    public String getRut_representante() {
        return rut_representante;
    }

    public void setRut_representante(String rut_representante) {
        this.rut_representante = rut_representante;
    }

    public String getRol_representante() {
        return rol_representante;
    }

    public void setRol_representante(String rol_representante) {
        this.rol_representante = rol_representante;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAppaterno() {
        return appaterno;
    }

    public void setAppaterno(String appaterno) {
        this.appaterno = appaterno;
    }

    public String getApmaterno() {
        return apmaterno;
    }

    public void setApmaterno(String apmaterno) {
        this.apmaterno = apmaterno;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public Date getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(Date fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public String getFono_1() {
        return fono_1;
    }

    public void setFono_1(String fono_1) {
        this.fono_1 = fono_1;
    }

    public String getFono_2() {
        return fono_2;
    }

    public void setFono_2(String fono_2) {
        this.fono_2 = fono_2;
    }

    public char getVigente() {
        return vigente;
    }

    public void setVigente(char vigente) {
        this.vigente = vigente;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    
}
