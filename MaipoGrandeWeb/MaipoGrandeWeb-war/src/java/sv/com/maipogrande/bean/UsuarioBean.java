/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.bean;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;
import org.jboss.weld.context.RequestContext;
import org.primefaces.PrimeFaces;
import sv.com.maipogrande.entities.TEmpresa;
import sv.com.maipogrande.entities.TRepresentante;
import sv.com.maipogrande.entities.TSubasta;
import sv.com.maipogrande.entities.TUsuario;
import sv.com.maipogrande.model.TEmpresaFacade;
import sv.com.maipogrande.model.TPedidoFacade;
import sv.com.maipogrande.model.TRepresentanteFacade;
import sv.com.maipogrande.model.TSubastaFacade;
import sv.com.maipogrande.model.TUsuarioFacade;
import sv.com.maipogrande.session.Util;

/**
 *
 * @author Maximiliano González
 */
@Named(value = "usuarioBean")
@RequestScoped
public class UsuarioBean {

    @EJB
    private TEmpresaFacade tEmpresaFacade;

    @EJB
    private TRepresentanteFacade tRepresentanteFacade;

    @EJB
    private TUsuarioFacade tUsuarioFacade;

    private TUsuario usuario;

    private BigDecimal idusuario;
    private String email;
    private String contrasena;
    private String contrasena2;
    private TRepresentante representante;
    private char vigente;

    //representante
    private BigDecimal id_representante;
    private String rut_representante;
    private String rol_representante;
    private String nombre;
    private String appaterno;
    private String apmaterno;
    private char sexo;
    private Date fecha_nacimiento;
    private String fono_1;
    private String fono_2;
    private String direccion;
    private char vigente2;
    private TEmpresa empresa;
    private BigDecimal idempresa;
    private List<TEmpresa> listatempempresa;
    private List<String> lstroles;

    public UsuarioBean() {
        usuario = new TUsuario();
        representante = new TRepresentante();
        empresa = new TEmpresa();

        lstroles = new ArrayList();
        lstroles.add("Cliente Int");
        lstroles.add("Cliente Ext");
    }

    public String getContrasena2() {
        return contrasena2;
    }

    public void setContrasena2(String contrasena2) {
        this.contrasena2 = contrasena2;
    }

    public List<TUsuario> getUsuarios() {
        return tUsuarioFacade.findAll();
    }

    public BigDecimal getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(BigDecimal idusuario) {
        this.idusuario = idusuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public TRepresentante getRepresentante() {
        return representante;
    }

    public void setRepresentante(TRepresentante representante) {
        this.representante = representante;
    }

    public char getVigente() {
        return vigente;
    }

    public void setVigente(char vigente) {
        this.vigente = vigente;
    }

    public TUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(TUsuario usuario) {
        this.usuario = usuario;
    }

    /////////REPRESENTANTE///////////
    public BigDecimal getId_representante() {
        return id_representante;
    }

    public void setId_representante(BigDecimal id_representante) {
        this.id_representante = id_representante;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getRut_representante() {
        return rut_representante;
    }

    public void setRut_representante(String rut_representante) {
        this.rut_representante = rut_representante;
    }

    public String getRol_representante() {
        return rol_representante;
    }

    public void setRol_representante(String rol_representante) {
        this.rol_representante = rol_representante;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAppaterno() {
        return appaterno;
    }

    public void setAppaterno(String appaterno) {
        this.appaterno = appaterno;
    }

    public String getApmaterno() {
        return apmaterno;
    }

    public void setApmaterno(String apmaterno) {
        this.apmaterno = apmaterno;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public Date getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(Date fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public String getFono_1() {
        return fono_1;
    }

    public void setFono_1(String fono_1) {
        this.fono_1 = fono_1;
    }

    public String getFono_2() {
        return fono_2;
    }

    public void setFono_2(String fono_2) {
        this.fono_2 = fono_2;
    }

    public char getVigente2() {
        return vigente2;
    }

    public void setVigente2(char vigente2) {
        this.vigente2 = vigente2;
    }

    public TEmpresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(TEmpresa empresa) {
        this.empresa = empresa;
    }

    public List<String> getLstroles() {
        return lstroles;
    }

    public void setLstroles(List<String> lstroles) {
        this.lstroles = lstroles;
    }

    public List<TEmpresa> getListatempempresa() {
        return listatempempresa;
    }

    public void setListatempempresa(List<TEmpresa> listatempempresa) {
        this.listatempempresa = listatempempresa;
    }

    public TRepresentante obtenerRepresentante() {
        TRepresentante rep = (TRepresentante) Util.getSession().getAttribute("representante");
        System.out.println("obtenerRepresent: " + rep);
        return tRepresentanteFacade.find(rep.getIdrepresentante());
    }

    public String crear() throws MessagingException {

        FacesContext context = FacesContext.getCurrentInstance();

        TRepresentante repp = new TRepresentante();
        repp.setIdrepresentante(id_representante);
        repp.setIdempresa(tEmpresaFacade.find(BigDecimal.valueOf(1)));// Se ingresa empresa generica MAIPO GRANDE
        repp.setRutrepresentante(rut_representante);
        System.out.println("" + representante.getRolrepresentante());
        System.out.println("" + this.representante.getRolrepresentante());
        repp.setRolrepresentante(representante.getRolrepresentante());
        repp.setNombre(nombre);
        repp.setApmaterno(apmaterno);
        repp.setAppaterno(appaterno);
        repp.setSexo(sexo);
        repp.setFechanacimiento(fecha_nacimiento);
        repp.setFono1(fono_1);
        repp.setDireccion(direccion);
        repp.setVigente('1');

        System.out.println("id empresa " + empresa.getIdempresa());
        System.out.println("rut " + rut_representante);
        System.out.println("rol " + rol_representante);
        System.out.println("nombre " + nombre);
        System.out.println("materno " + apmaterno);
        System.out.println("paterno " + appaterno);
        System.out.println("sexo " + sexo);
        System.out.println("fecha " + fecha_nacimiento);
        System.out.println("fono " + fono_1);
        System.out.println("estado " + vigente);

        TUsuario u = new TUsuario();
        u.setIdusuario(idusuario);
        u.setEmail(email);
        u.setContrasena(contrasena);
        u.setIdrepresentante(repp);
        u.setVigente('1');

        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Usuario creado con éxito: " + u.getEmail() + " ", "Usuario creado"));
        enviarCorreoInicial(rut_representante, nombre, appaterno, apmaterno, email);
        tRepresentanteFacade.create(repp);
        tUsuarioFacade.create(u);

        PrimeFaces current = PrimeFaces.current();
        //  current.executeScript("PF('myDialogVar').Hide();");
        //limpiar
        limpiar();

        return "login";
    }

    public void limpiar() {
        email = null;
        nombre = null;
        apmaterno = null;
        appaterno = null;
        rut_representante = null;
        fecha_nacimiento = null;
        contrasena = null;
        contrasena2 = null;
        fono_1 = null;
        rol_representante = null;
        direccion=null;
        sexo = '\0';
        empresa = null;

    }

    public boolean editar() {
        TRepresentante repp = new TRepresentante();
        TUsuario u = new TUsuario();
        String fEmail = email;

        //busqueda
        for (TUsuario usr : this.getUsuarios()) {
            if (usr.getEmail().equals(fEmail)) {
                u = usr;
                break;
            }
        }

        repp = u.getIdrepresentante();

        repp.setIdempresa(empresa);
        repp.setRutrepresentante(rut_representante);
        repp.setNombre(nombre);
        repp.setApmaterno(apmaterno);
        repp.setAppaterno(appaterno);
        repp.setSexo(sexo);
        repp.setFechanacimiento(fecha_nacimiento);
        repp.setFono1(fono_1);
        repp.setDireccion(direccion);
        tRepresentanteFacade.edit(repp);

        u.setEmail(email);
        u.setContrasena(contrasena);
        u.setIdrepresentante(repp);
        tUsuarioFacade.edit(u);

        return true;
    }

    public void llenarlista() {
        //conseguir email de la session
        HttpSession hs = Util.getSession();
        String fEmail = hs.getAttribute("email").toString();

        //String fEmail = "adminjp@gmail.com";
        TUsuario usuario = new TUsuario();

        //metodo de busqueda antiguo lento
        for (TUsuario usr : this.getUsuarios()) {
            if (usr.getEmail().equals(fEmail)) {
                usuario = usr;
                break;
            }
        }

        //TUsuario usuario = tUsuarioFacade.findByEmail(fEmail); <--- metodo mediante EJB incompleto
        this.email = usuario.getEmail();
        this.id_representante = usuario.getIdrepresentante().getIdrepresentante();
        this.rut_representante = usuario.getIdrepresentante().getRutrepresentante();
        this.nombre = usuario.getIdrepresentante().getNombre();
        this.appaterno = usuario.getIdrepresentante().getAppaterno();
        this.apmaterno = usuario.getIdrepresentante().getApmaterno();
        this.sexo = usuario.getIdrepresentante().getSexo();
        this.fecha_nacimiento = usuario.getIdrepresentante().getFechanacimiento();
        this.fono_1 = usuario.getIdrepresentante().getFono1();
        this.fono_2 = usuario.getIdrepresentante().getFono2();
        this.direccion = usuario.getIdrepresentante().getDireccion();
        this.empresa = usuario.getIdrepresentante().getIdempresa();

    }

    public TUsuario obtenerUsuario() {
        if (usuario == null) {
            usuario = (TUsuario) Util.getSession().getAttribute("usuario");
            usuario = tUsuarioFacade.find(usuario.getIdusuario());
        }
        return usuario;
    }

    public List<TEmpresa> getEmpresas() {

        return tEmpresaFacade.findAll();
    }

    public List<TRepresentante> getRepresentantes() {
        return tRepresentanteFacade.findAll();
    }

    /*
    public void handleEmpresa2() {

        if (this.representante.getRolrepresentante() != null && !this.representante.getRolrepresentante().equals(0)) {
            this.listatempempresa = new ArrayList<>();

            for (TEmpresa emp : this.getEmpresas()) {
                if (emp.getTipoempresa().equals(this.representante.getRolrepresentante())) {

                    empresa = emp;
                    this.listatempempresa.add(emp);

                }
                if (emp.getTipoempresa().equals("Cliente") && this.representante.getRolrepresentante().equals("Cliente Int")) {

                    this.listatempempresa.add(emp);

                }
                if (emp.getTipoempresa().equals("Cliente") && this.representante.getRolrepresentante().equals("Cliente Ext")) {

                    this.listatempempresa.add(emp);
                }

            }

        }
    }
*/
    // Método para enviar un correo al usuario que se ha ingresado en el sistema
    // Creado por Cristián Esquivel
    public void enviarCorreoInicial(String rut, String nombre, String appat, String apmat, String mail) throws MessagingException {
        Properties prop = new Properties();
        prop.setProperty("mail.smtp.host", "smtp.gmail.com");
        prop.setProperty("mail.smtp.starttls.enable", "true");
        prop.setProperty("mail.smtp.port", "587");
        prop.setProperty("mail.smtp.auth", "true");

        Session sesion = Session.getDefaultInstance(prop);
        String correoEmisor = "maipogrande.feriavirtual@gmail.com";
        String contra = "Maipo123";
        String destinatario = mail;
        String asunto = "MAIPO GRANDE - Registro de Usuario";
        String mensaje = "Estimado/a " + nombre + " " + appat + " " + apmat + " RUT: " + rut + ".\n"
                + "\n"
                + "Gracias por registrarse en Maipo Grande, ahora puede ingresar en nuestra plataforma con su email y contraseña especificada en el ingreso.\n"
                + "\n"
                + "Saludos.";

        MimeMessage m = new MimeMessage(sesion);
        try {
            m.setFrom(new InternetAddress(correoEmisor));
            m.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
            m.setSubject(asunto);
            m.setText(mensaje);

            try (Transport t = sesion.getTransport("smtp")) {
                t.connect(correoEmisor, contra);
                t.sendMessage(m, m.getRecipients(Message.RecipientType.TO));
            }
        } catch (AddressException ex) {
            Logger.getLogger(UsuarioBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}