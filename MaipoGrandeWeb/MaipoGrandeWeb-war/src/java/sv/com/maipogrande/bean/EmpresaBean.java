/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.bean;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import sv.com.maipogrande.entities.TEmpresa;
import sv.com.maipogrande.model.TEmpresaFacade;

/**
 *
 * @author Maximiliano González
 */
@Named(value = "empresaBean")
@RequestScoped
public class EmpresaBean {

    @EJB
    private TEmpresaFacade tEmpresaFacade;
    
    
    private BigDecimal idempresa;
    private BigDecimal rutempresa;
    private String nombrecomercial; 
    private String tipoempresa;
    private String razonsocial;
    private String direccion;
    private String ciudad;
    private String pais;
    private BigDecimal number;
    private String email;
    
    
    
    public EmpresaBean() {
        
    }
    
        public List<TEmpresa> getEmpresas() {
        return tEmpresaFacade.findAll();
    }

    public BigDecimal getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(BigDecimal idempresa) {
        this.idempresa = idempresa;
    }
        

    public BigDecimal getRutempresa() {
        return rutempresa;
    }

    public void setRutempresa(BigDecimal rutempresa) {
        this.rutempresa = rutempresa;
    }

    public String getNombrecomercial() {
        return nombrecomercial;
    }

    public void setNombrecomercial(String nombrecomercial) {
        this.nombrecomercial = nombrecomercial;
    }

    public String getTipoempresa() {
        return tipoempresa;
    }

    public void setTipoempresa(String tipoempresa) {
        this.tipoempresa = tipoempresa;
    }

    public String getRazonsocial() {
        return razonsocial;
    }

    public void setRazonsocial(String razonsocial) {
        this.razonsocial = razonsocial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public BigDecimal getNumber() {
        return number;
    }

    public void setNumber(BigDecimal number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
}
