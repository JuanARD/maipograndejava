/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.controller;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import sv.com.maipogrande.entities.TSubasta;
import sv.com.maipogrande.model.TSubastaFacade;
import sv.com.maipogrande.entities.TRepresentante;
import sv.com.maipogrande.model.TRepresentanteFacade;
import sv.com.maipogrande.entities.TPedido;
import sv.com.maipogrande.model.TPedidoFacade;

/**
 *
 * @author Juan Hernández
 */
@Named(value = "subastaController")
@SessionScoped
public class SubastaController implements Serializable {

    @EJB
    private TSubastaFacade tSubastaFacade;

    @EJB
    private TRepresentanteFacade tRepresentanteFacade;

    @EJB
    private TPedidoFacade tPedidoFacade;

    private TSubasta subasta;

    private TRepresentante representante;

    private TPedido pedido;

    private List<SubastaSeleccionado> subastaSeleccionado;

    /**
     * Creates a new instance of SubastaController
     */
    public SubastaController() {
        subasta = new TSubasta();
        representante = new TRepresentante();
        pedido = new TPedido();
        subastaSeleccionado = new ArrayList<SubastaSeleccionado>();
    }

    public class SubastaSeleccionado {

        private int idsubasta;
        private int idpedido;
        private String nombreRepre;
        private String nombresubasta;
        private String fechapublic;
        private String fechaTermino;

        public SubastaSeleccionado() {
        }

        public int getIdsubasta() {
            return idsubasta;
        }

        public void setIdsubasta(int idsubasta) {
            this.idsubasta = idsubasta;
        }

        public String getNombresubasta() {
            return nombresubasta;
        }

        public void setNombresubasta(String nombresubasta) {
            this.nombresubasta = nombresubasta;
        }

        public String getNombreRepre() {
            return nombreRepre;
        }

        public void setNombreRepre(String nombreRepre) {
            this.nombreRepre = nombreRepre;
        }

        public int getIdpedido() {
            return idpedido;
        }

        public void setIdpedido(int idpedido) {
            this.idpedido = idpedido;
        }

        public String getFechapublic() {
            return fechapublic;
        }

        public void setFechapublic(String fechapublic) {
            this.fechapublic = fechapublic;
        }

        public String getFechaTermino() {
            return fechaTermino;
        }

        public void setFechaTermino(String fechaTermino) {
            this.fechaTermino = fechaTermino;
        }
    }

    public TSubasta getSubasta() {
        return subasta;
    }

    public void setSubasta(TSubasta subasta) {
        this.subasta = subasta;
    }

    public TRepresentante getRepresentante() {
        return representante;
    }

    public void setRepresentante(TRepresentante representante) {
        this.representante = representante;
    }

    public TPedido getPedido() {
        return pedido;
    }

    public void setPedido(TPedido pedido) {
        this.pedido = pedido;
    }

    public List<TSubasta> listarTodosSubastas() {
        return tSubastaFacade.findAll();
    }

    public List<SubastaSeleccionado> getSubastaSeleccionado() {
        return subastaSeleccionado;
    }

    public void setSubastaSeleccionado(List<SubastaSeleccionado> subastaSeleccionado) {
        this.subastaSeleccionado = subastaSeleccionado;
    }

    public String addSubasta() {
        tSubastaFacade.create(subasta);
        this.subasta = new TSubasta();
        return "crearSubasta";
    }

    public String prepareEditSubasta(TSubasta s) {
        this.subasta = s;
        return "#";
    }

    public String editarSubasta() {
        this.tSubastaFacade.edit(subasta);
        this.subasta = new TSubasta();
        return "#";
    }

    public void deleteSubasta(TSubasta s) {
        this.tSubastaFacade.remove(s);
    }

    public String parseFechaPublicacion() {
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
        String date = DATE_FORMAT.format(subasta.getFechapublicacion());
        return date;
    }

    public List<TPedido> listarPedido() {
        return tPedidoFacade.findAll();
    }

    public void addToSubasta() {
        FacesContext context = FacesContext.getCurrentInstance();
        SubastaSeleccionado s = new SubastaSeleccionado();
        s.setIdsubasta(Integer.parseInt(subasta.getIdsubasta().toString()));
        s.setIdpedido(Integer.parseInt(pedido.getIdpedido().toString()));
        s.setNombreRepre(representante.getNombre());
        s.setFechapublic(subasta.getFechapublicacion().toString());
        s.setFechaTermino(subasta.getFechatermino().toString());
        try {
            subastaSeleccionado.add(s);
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
    }

    public String prepareEditSubastaOferta(TSubasta s) {
        this.subasta = s;
        return "crearOferta?faces-redirect=true";
    }

}
