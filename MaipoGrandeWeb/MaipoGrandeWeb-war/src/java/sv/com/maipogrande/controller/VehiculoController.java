/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.controller;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;
import sv.com.maipogrande.entities.TEmpresa;
import sv.com.maipogrande.entities.TLoteProducto;
import sv.com.maipogrande.entities.TOfertaTransporte;
import sv.com.maipogrande.entities.TProducto;
import sv.com.maipogrande.entities.TSubasta;
import sv.com.maipogrande.entities.TUnidadMedicion;
import sv.com.maipogrande.entities.TUsuario;
import sv.com.maipogrande.entities.TVehiculo;
import sv.com.maipogrande.model.TEmpresaFacade;
import sv.com.maipogrande.model.TUsuarioFacade;
import sv.com.maipogrande.model.TVehiculoFacade;
import static sv.com.maipogrande.scripts.Funciones.obtenerIdDeClase;
import sv.com.maipogrande.session.Util;

/**
 *
 * @author cems
 */
@Named(value = "vehiculoController")
@SessionScoped
public class VehiculoController implements Serializable {

    @EJB
    private TUsuarioFacade tUsuarioFacade;

    @EJB
    private TEmpresaFacade tEmpresaFacade;

    @EJB
    private TVehiculoFacade TVehiculoFacade;

    private TVehiculo TVehiculo;
    private TEmpresa TEmpresa;
   

    private long idvehiculo;
    private long idempresa;
    private String patente;
    private String clase;
    private long capacidadkg;
    private long cmancho;
    private long cmalto;
    private long cmlargo;
    private char frigorifico;

    public VehiculoController() {
        TEmpresa = new TEmpresa();
    }

    public List<TVehiculo> getTVehiculos() {
        return TVehiculoFacade.findAll();
    }
    
    
    
    private TUsuario usuario;

    public List<TVehiculo> listarVehiculo() {
        return TVehiculoFacade.filtrarVehiculo(obtenerUsuario().getIdrepresentante().getIdempresa());
    }

    TUsuario obtenerUsuario() {//método de obtener usuario - autor cristian esquivel
        usuario = (TUsuario) Util.getSession().getAttribute("usuario");
        usuario = tUsuarioFacade.find(usuario.getIdusuario());

        return usuario;
    }

    
    
    
    
    
    
    
    
    public List<TEmpresa> listarEmpresa() {
        return tEmpresaFacade.findAll();
    }

    public String ingresarVehiculo() {
        FacesContext context = FacesContext.getCurrentInstance();

        try {

            if (patente.isEmpty()) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Debe ingresar patente del vehículo.!"));
                return null;
            }
            if (clase.isEmpty()) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Se debe ingresar clase de vehículo.!"));
                return null;
            }
            if (capacidadkg <= 0) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Se debe ingresar capacidad KG.!"));
                return null;
            }
            if (cmancho <= 0) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Se debe ingresar cm de ancho.!"));
                return null;
            }
            if (cmalto <= 0) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Se debe ingresar cm de alto.!"));
                return null;
            }
            if (cmlargo <= 0) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Se debe ingresar cm de largo.!"));
                return null;
            }

            TVehiculo tv = new TVehiculo();

            //sesion para obtener empresa del a base del usuario 
            TUsuario usuario = (TUsuario) Util.getSession().getAttribute("usuario");
            usuario = tUsuarioFacade.find(usuario.getIdusuario());
            tv.setIdempresa(usuario.getIdrepresentante().getIdempresa());

            tv.setPatente(patente);
            tv.setClase(clase);
            tv.setCapacidadkg(BigInteger.valueOf(capacidadkg));
            tv.setCmancho(BigInteger.valueOf(cmancho));
            tv.setCmalto(BigInteger.valueOf(cmalto));
            tv.setCmlargo(BigInteger.valueOf(cmlargo));
            tv.setFrigorifico(frigorifico);

            TVehiculoFacade.create(tv);
            context.addMessage(null, new FacesMessage("", "Vehículo Ingresado Correctamente :)!"));

        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }

        limpiarVehículo();
        return "Vehiculo?faces-redirect=true";

    }


    public void limpiarVehículo() {

        this.patente = "";
        this.clase = "";
        this.capacidadkg = 0;
        this.cmancho = 0;
        this.cmalto = 0;
        this.cmlargo = 0;
        this.frigorifico = 1;
    }

    public String prepararModificar(TVehiculo v) {
        this.TVehiculo = v;
        return "modificarVehiculo?faces-redirect=true";
    }

    public String modificarVehiculo() {

        this.TVehiculoFacade.edit(TVehiculo);
        this.TVehiculo = new TVehiculo();

        return "Vehiculo?faces-redirect=true";

    }

    public String FrigorificoV(char frigo) {
        String fri = "";
        if (frigo == '1') {
            fri = "Si";
        } else {
            fri = "No";
        }
        return fri;
    }

    public String eliminarVehiculo(TVehiculo v) {
        this.TVehiculoFacade.remove(v);
        return "Vehiculo?faces-redirect=true";
    }

    //GET AND SETTER
    public TEmpresaFacade gettEmpresaFacade() {
        return tEmpresaFacade;
    }

    public void settEmpresaFacade(TEmpresaFacade tEmpresaFacade) {
        this.tEmpresaFacade = tEmpresaFacade;
    }

    public TVehiculoFacade getTVehiculoFacade() {
        return TVehiculoFacade;
    }

    public void setTVehiculoFacade(TVehiculoFacade TVehiculoFacade) {
        this.TVehiculoFacade = TVehiculoFacade;
    }

    public TVehiculo getTVehiculo() {
        return TVehiculo;
    }

    public void setTVehiculo(TVehiculo TVehiculo) {
        this.TVehiculo = TVehiculo;
    }

    public TEmpresa getTEmpresa() {
        return TEmpresa;
    }

    public void setTEmpresa(TEmpresa TEmpresa) {
        this.TEmpresa = TEmpresa;
    }

    public long getIdvehiculo() {
        return idvehiculo;
    }

    public void setIdvehiculo(long idvehiculo) {
        this.idvehiculo = idvehiculo;
    }

    public long getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(long idempresa) {
        this.idempresa = idempresa;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public long getCapacidadkg() {
        return capacidadkg;
    }

    public void setCapacidadkg(long capacidadkg) {
        this.capacidadkg = capacidadkg;
    }

    public long getCmancho() {
        return cmancho;
    }

    public void setCmancho(long cmancho) {
        this.cmancho = cmancho;
    }

    public long getCmalto() {
        return cmalto;
    }

    public void setCmalto(long cmalto) {
        this.cmalto = cmalto;
    }

    public long getCmlargo() {
        return cmlargo;
    }

    public void setCmlargo(long cmlargo) {
        this.cmlargo = cmlargo;
    }

    public char getFrigorifico() {
        return frigorifico;
    }

    public void setFrigorifico(char frigorifico) {
        this.frigorifico = frigorifico;
    }

}
