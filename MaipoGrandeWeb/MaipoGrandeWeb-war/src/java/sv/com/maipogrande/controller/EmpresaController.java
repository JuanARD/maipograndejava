/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.controller;

import java.math.BigDecimal;
import java.sql.Blob;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.servlet.http.HttpSession;
import sv.com.maipogrande.entities.TEmpresa;
import sv.com.maipogrande.entities.TUsuario;
import sv.com.maipogrande.model.TEmpresaFacade;
import sv.com.maipogrande.model.TRepresentanteFacade;
import sv.com.maipogrande.model.TUsuarioFacade;
import sv.com.maipogrande.scripts.Funciones;
import static sv.com.maipogrande.scripts.Funciones.obtenerIdDeClase;
import sv.com.maipogrande.session.Util;

/**
 *
 * @author cems
 */
@Named(value = "empresaController")
@Dependent
public class EmpresaController {

    @EJB
    private TRepresentanteFacade tRepresentanteFacade;
    @EJB
    private TUsuarioFacade tUsuarioFacade;
    @EJB
    private TEmpresaFacade tEmpresaFacade;
    
    
    private TEmpresa empresa;
    private TUsuario usuario;

    public TUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(TUsuario usuario) {
        this.usuario = usuario;
    }

    public TEmpresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(TEmpresa empresa) {
        this.empresa = empresa;
    }

    public BigDecimal getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(BigDecimal idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getRutEmpresa() {
        return rutEmpresa;
    }

    public void setRutEmpresa(String rutEmpresa) {
        this.rutEmpresa = rutEmpresa;
    }

    public String getTipoEmpresa() {
        return tipoEmpresa;
    }

    public void setTipoEmpresa(String tipoEmpresa) {
        this.tipoEmpresa = tipoEmpresa;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public Blob getLogo() {
        return logo;
    }

    public void setLogo(Blob logo) {
        this.logo = logo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getFono1() {
        return fono1;
    }

    public void setFono1(String fono1) {
        this.fono1 = fono1;
    }

    public String getFono2() {
        return fono2;
    }

    public void setFono2(String fono2) {
        this.fono2 = fono2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private BigDecimal idEmpresa;
    private String rutEmpresa;
    private String tipoEmpresa;
    private String nombreComercial;
    private String razonSocial;
    private Blob logo;
    private String direccion;
    private String ciudad;
    private String pais;
    private String fono1;
    private String fono2;
    private String email;
           
    public EmpresaController() {
    }
    
    
    public List<TEmpresa>getTEmpresa()
    {
       return tEmpresaFacade.findAll();
    }
    
    public TEmpresa obtenerEmpresaLoqueada(){
        HttpSession hs = Util.getSession(); int id=0;
        try {
                id = obtenerIdDeClase(hs.getAttribute("idEmpresa").toString());
                empresa = tEmpresaFacade.find(BigDecimal.valueOf(id));    
        } catch (Exception e) {
            throw e;
        }
        return empresa;
    }
    
    public String obtenerNombreEmpresaLogueada(){
        HttpSession hs = Util.getSession();
        String nombre, ruta; int id; 
        try {
            id = obtenerIdDeClase(hs.getAttribute("idEmpresa").toString());
            nombre = tEmpresaFacade.find(BigDecimal.valueOf(id)).getNombrecomercial();
            return nombre;
        } catch (Exception e) {
            return "Error";
        }
    }
}
