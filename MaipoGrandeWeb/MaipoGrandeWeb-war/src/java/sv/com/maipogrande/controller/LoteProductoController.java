/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.controller;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import sv.com.maipogrande.entities.TEmpresa;
import sv.com.maipogrande.entities.TLoteOfrecido;
import sv.com.maipogrande.entities.TLoteProducto;
import sv.com.maipogrande.entities.TPedido;
import sv.com.maipogrande.entities.TProducto;
import sv.com.maipogrande.entities.TProductoPedido;
import sv.com.maipogrande.entities.TRepresentante;
import sv.com.maipogrande.entities.TUnidadMedicion;
import sv.com.maipogrande.entities.TUsuario;
import sv.com.maipogrande.model.TEmpresaFacade;
import sv.com.maipogrande.model.TLoteOfrecidoFacade;
import sv.com.maipogrande.model.TLoteProductoFacade;
import sv.com.maipogrande.model.TPedidoFacade;
import sv.com.maipogrande.model.TProductoFacade;
import sv.com.maipogrande.model.TProductoPedidoFacade;
import sv.com.maipogrande.model.TRepresentanteFacade;
import sv.com.maipogrande.model.TUnidadMedicionFacade;
import sv.com.maipogrande.model.TUsuarioFacade;
import static sv.com.maipogrande.scripts.Funciones.obtenerIdDeClase;
import sv.com.maipogrande.session.Util;

/**
 *
 * @author Leb
 */
@Named(value = "loteProductoController")
@SessionScoped
public class LoteProductoController implements Serializable {

    //Facades para acceder a las clases de la base de datos
    @EJB
    private TUnidadMedicionFacade tUnidadMedicionFacade;
    @EJB
    private TEmpresaFacade tEmpresaFacade;
    @EJB
    private TProductoFacade tProductoFacade;
    @EJB
    private TLoteProductoFacade tLoteProductoFacade;
    @EJB
    private TRepresentanteFacade tRepresentanteFacade;
    @EJB
    private TUsuarioFacade tUsuarioFacade;
    @EJB
    private TLoteOfrecidoFacade tLoteOfrecidoFacade;
    @EJB
    private TProductoPedidoFacade tProductoPedidoFacade;
    @EJB
    private TPedidoFacade tPedidoFacade;

    //Instancias de clases y variables temporales
    private TLoteProducto TLoteProducto;
    private TProductoPedido productoPedido;
    private TPedido pedido;
    private TLoteOfrecido loteOfrecido;
    private TProducto TProducto;
    private TEmpresa TEmpresa;
    private TUnidadMedicion TUnidadMedicion;
    private TUsuario usuario;

    private long idLoteProducto;
    private long idProducto;
    private long idEmpresa;
    private byte unidadMedicion;
    private long cantidadProductos;
    private long precioUnidad;
    private byte calidad;
    private Date fechaVencimiento;
    private String condicionesAlmacenaje;
    private char ventaExtranjero;
    private String descripcion;
    private char vigente;

    private boolean seleccionarProducto;

    //Constructor
    public LoteProductoController() {
        TUnidadMedicion = new TUnidadMedicion();
        TLoteProducto = new TLoteProducto();
        TProducto = new TProducto();
    }

    //Getter and Setter
    public TUnidadMedicionFacade gettUnidadMedicionFacade() {
        return tUnidadMedicionFacade;
    }

    public void settUnidadMedicionFacade(TUnidadMedicionFacade tUnidadMedicionFacade) {
        this.tUnidadMedicionFacade = tUnidadMedicionFacade;
    }

    public TProductoPedido getProductoPedido() {
        return productoPedido;
    }

    public void setProductoPedido(TProductoPedido productoPedido) {
        this.productoPedido = productoPedido;
    }

    public TPedido getPedido() {
        return pedido;
    }

    public void setPedido(TPedido pedido) {
        this.pedido = pedido;
    }

    public TLoteOfrecido getLoteOfrecido() {
        return loteOfrecido;
    }

    public void setLoteOfrecido(TLoteOfrecido loteOfrecido) {
        this.loteOfrecido = loteOfrecido;
    }

    public TUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(TUsuario usuario) {
        this.usuario = usuario;
    }

    public TEmpresaFacade gettEmpresaFacade() {
        return tEmpresaFacade;
    }

    public void settEmpresaFacade(TEmpresaFacade tEmpresaFacade) {
        this.tEmpresaFacade = tEmpresaFacade;
    }

    public TProductoFacade gettProductoFacade() {
        return tProductoFacade;
    }

    public void settProductoFacade(TProductoFacade tProductoFacade) {
        this.tProductoFacade = tProductoFacade;
    }

    public TRepresentanteFacade gettRepresentanteFacade() {
        return tRepresentanteFacade;
    }

    public void settRepresentanteFacade(TRepresentanteFacade tRepresentanteFacade) {
        this.tRepresentanteFacade = tRepresentanteFacade;
    }

    public TLoteProducto getTLoteProducto() {
        return TLoteProducto;
    }

    public void setTLoteProducto(TLoteProducto TLoteProducto) {
        this.TLoteProducto = TLoteProducto;
    }

    public TProducto getTProducto() {
        return TProducto;
    }

    public void setTProducto(TProducto TProducto) {
        this.TProducto = TProducto;
    }

    public TEmpresa getTEmpresa() {
        return TEmpresa;
    }

    public void setTEmpresa(TEmpresa TEmpresa) {
        this.TEmpresa = TEmpresa;
    }

    public TUnidadMedicion getTUnidadMedicion() {
        return TUnidadMedicion;
    }

    public void setTUnidadMedicion(TUnidadMedicion TUnidadMedicion) {
        this.TUnidadMedicion = TUnidadMedicion;
    }

    public long getIdLoteProducto() {
        return idLoteProducto;
    }

    public void setIdLoteProducto(long idLoteProducto) {
        this.idLoteProducto = idLoteProducto;
    }

    public long getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(long idProducto) {
        this.idProducto = idProducto;
    }

    public long getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(long idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public byte getUnidadMedicion() {
        return unidadMedicion;
    }

    public void setUnidadMedicion(byte unidadMedicion) {
        this.unidadMedicion = unidadMedicion;
    }

    public long getCantidadProductos() {
        return cantidadProductos;
    }

    public void setCantidadProductos(long cantidadProductos) {
        this.cantidadProductos = cantidadProductos;
    }

    public float getPrecioUnidad() {
        return precioUnidad;
    }

    public void setPrecioUnidad(int precioUnidad) {
        this.precioUnidad = precioUnidad;
    }

    public byte getCalidad() {
        return calidad;
    }

    public void setCalidad(byte calidad) {
        this.calidad = calidad;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getCondicionesAlmacenaje() {
        return condicionesAlmacenaje;
    }

    public void setCondicionesAlmacenaje(String condicionesAlmacenaje) {
        this.condicionesAlmacenaje = condicionesAlmacenaje;
    }

    public char getVentaExtranjero() {
        return ventaExtranjero;
    }

    public void setVentaExtranjero(char ventaExtranjero) {
        this.ventaExtranjero = ventaExtranjero;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public char getVigente() {
        return vigente;
    }

    public void setVigente(char vigente) {
        this.vigente = vigente;
    }

    public boolean isSeleccionarProducto() {
        return seleccionarProducto;
    }

    public void setSeleccionarProducto(boolean seleccionarProducto) {
        this.seleccionarProducto = seleccionarProducto;
    }

    public TLoteProducto selectLoteProducto() {
        return this.TLoteProducto;
    }

    //Listas
    public List<TLoteProducto> listarTodosLotesProductos() {
        return tLoteProductoFacade.findAll();
    }

    public List<TProducto> listarTodosProductos() {
        return tProductoFacade.findAll();
    }

    public List<TUnidadMedicion> listarUnidadesMedicion() {
        return tUnidadMedicionFacade.findAll();
    }

    public List<TProducto> listarFrutasPorNombres() {
        return tProductoFacade.findAll();
    }

    TUsuario obtenerUsuario() {
        usuario = (TUsuario) Util.getSession().getAttribute("usuario");
        usuario = tUsuarioFacade.find(usuario.getIdusuario());
        return usuario;
    }

    /**
     * Lista por representante
     * @return 
     */
    public List<TLoteOfrecido> ListarLoteOfrecidosP() {
        return tLoteOfrecidoFacade.findLoteOfrecidoByRepresentante(obtenerUsuario().getIdrepresentante());
    }

    public List<TLoteProducto> listarLotesRepresentante() {
        return tLoteProductoFacade.listarLotesRepresentante(obtenerUsuario().getIdrepresentante().getIdempresa());
    }


//    public List<TProducto> listarFrutasPorNombre() {
//
//        List<TProducto> frutas = new ArrayList();
//        for (int i = 0; i < tProductoFacade.count(); i++) {
//            for (int j = 0; j < tProductoFacade.count(); j++) {
//                if (frutas == null) {
//                    frutas.add(tProductoFacade.find(i));
//                } else if (frutas.get(i).getNombre() != frutas.get(j).getNombre()) {
//                    frutas.add(tProductoFacade.find(i));
//                }
//            }
//        }
//        return frutas;
//    }
    public void crear() {
        FacesContext context = FacesContext.getCurrentInstance();
        Calendar cal = Calendar.getInstance();
        Date hoy = cal.getTime();

        try {
            //aunque aquí es casi imposible que quede null, lo dejé porque si
            if (TProducto == null) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Debe agregar un producto al lote."));
                return;
            }

            /* Esta parte no la veo necesaria, siendo que ya se puede saber la empresa desde el usuario que está iniciado sesión
            if (TEmpresa.getIdempresa() == null) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "¡NO SE ENCUENTRA PRODUCTOR!"));
                return;
            }*/
            if (cantidadProductos <= 0) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Debe agregar cantidad de productos."));
                return;
            }
            if (precioUnidad <= 0) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Debe agregar precio al lote."));
                return;
            }
            if (fechaVencimiento.before(hoy) || fechaVencimiento == null) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "La fecha no puede ser inferior a la actual."));
                return;
            }
//            if (condicionesAlmacenaje.equals("2") || condicionesAlmacenaje != null) {
//                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Debe seleccionar opción para frigorífico."));
//                return;
//            }
//            if (ventaExtranjero =='2' || ventaExtranjero == '\u0000'){
//                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Debe seleccionar opción para mercado de venta (al extranjero o local)"));
//                return;
//            }

            TLoteProducto pr = new TLoteProducto();
            //pr.setIdloteproducto(BigDecimal.valueOf(idLoteProducto)); Esto se debe llenar solo con la secuencia
            idProducto = obtenerIdDeClase(TProducto.getIdproducto().toString());
            pr.setIdproducto(tProductoFacade.find(BigDecimal.valueOf(idProducto)));

            //así es la forma de extraer cosas desde el usuario de la sesión, para que lo consideres mejor, se añadió el TUsuarioFacade, se necesita para extraer el usuario
            TUsuario usuario = (TUsuario) Util.getSession().getAttribute("usuario");
            usuario = tUsuarioFacade.find(usuario.getIdusuario());
            pr.setIdempresa(usuario.getIdrepresentante().getIdempresa());
            //No hay para que darse tantas vueltas haciendo .ToString y cosas así, así de simple se puede extraer la unidad de medición
            pr.setIdunidad(tUnidadMedicionFacade.find(TUnidadMedicion.getIdunidad()));

            pr.setCantidadproductos(BigInteger.valueOf(cantidadProductos));
            pr.setPreciounidad(BigInteger.valueOf(precioUnidad));
            pr.setCalidad(BigInteger.valueOf(calidad));
            pr.setFechavencimiento(fechaVencimiento);
            pr.setCondicionesalmacenaje(condicionesAlmacenaje);
            pr.setVentaalextranjero(ventaExtranjero);
            pr.setDescripcion(descripcion);
            pr.setVigente('1');
            tLoteProductoFacade.create(pr);
            context.addMessage(null, new FacesMessage("", "Lote de productos ingresado correctamente"));

            clearLoteProducto();
            return;

        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
        return;
    }

    public void clearLoteProducto() {
        Calendar cal = Calendar.getInstance();

        this.TUnidadMedicion = new TUnidadMedicion();
        this.TLoteProducto = new TLoteProducto();
        this.TProducto = new TProducto();

        this.cantidadProductos = 0;
        this.precioUnidad = 0;
        this.calidad = 0;
        this.fechaVencimiento = cal.getTime();
        this.condicionesAlmacenaje = "1";
        this.ventaExtranjero = '1';
        this.descripcion = null;
        this.vigente = '1';

        this.seleccionarProducto = false;
    }

    public void eliminarLoteProducto(TLoteProducto lp) {
        this.tLoteProductoFacade.remove(lp);
    }

    public String prepareEditLoteProducto(TLoteProducto lp) {
        this.TLoteProducto = lp;
        return "modificarLotes?faces-redirect=true";
    }

    public String editarLoteProducto() {
        this.tLoteProductoFacade.edit(TLoteProducto);
        this.TLoteProducto = new TLoteProducto();
        return "gestionarLotes?faces-redirect=true";
    }

    //Clase de carrito
    public class LoteProductoSeleccionado {

        @EJB
        private TUnidadMedicionFacade tUnidadMedicionFacade;
        @EJB
        private TEmpresaFacade tEmpresaFacade;
        @EJB
        private TProductoFacade tProductoFacade;
        @EJB
        private TLoteProductoFacade tLoteProductoFacade;
        @EJB
        private TRepresentanteFacade tRepresentanteFacade;
        @EJB
        private TUsuarioFacade tUsuarioFacade;

        private TLoteProducto TLoteProducto;
        private TProducto TProducto;
        private TEmpresa TEmpresa;
        private TUnidadMedicion TUnidadMedicion;

        private long idLoteProducto;
        private long idProducto;
        private long idEmpresa;
        private byte unidadMedicion;
        private long cantidadProductos;
        private float precioUnidad;
        private byte calidad;
        private Date fechaVencimiento;
        private String condicionesAlmacenaje;
        private char ventaExtranjero;
        private String descripcion;
        private char vigente;

        public TLoteProducto getTLoteProducto() {
            return TLoteProducto;
        }

        public void setTLoteProducto(TLoteProducto TLoteProducto) {
            this.TLoteProducto = TLoteProducto;
        }

        public TProducto getTProducto() {
            return TProducto;
        }

        public void setTProducto(TProducto TProducto) {
            this.TProducto = TProducto;
        }

        public TEmpresa getTEmpresa() {
            return TEmpresa;
        }

        public void setTEmpresa(TEmpresa TEmpresa) {
            this.TEmpresa = TEmpresa;
        }

        public TUnidadMedicion getTUnidadMedicion() {
            return TUnidadMedicion;
        }

        public void setTUnidadMedicion(TUnidadMedicion TUnidadMedicion) {
            this.TUnidadMedicion = TUnidadMedicion;
        }

        public long getIdLoteProducto() {
            return idLoteProducto;
        }

        public void setIdLoteProducto(long idLoteProducto) {
            this.idLoteProducto = idLoteProducto;
        }

        public long getIdProducto() {
            return idProducto;
        }

        public void setIdProducto(long idProducto) {
            this.idProducto = idProducto;
        }

        public long getIdEmpresa() {
            return idEmpresa;
        }

        public void setIdEmpresa(long idEmpresa) {
            this.idEmpresa = idEmpresa;
        }

        public byte getUnidadMedicion() {
            return unidadMedicion;
        }

        public void setUnidadMedicion(byte unidadMedicion) {
            this.unidadMedicion = unidadMedicion;
        }

        public long getCantidadProductos() {
            return cantidadProductos;
        }

        public void setCantidadProductos(long cantidadProductos) {
            this.cantidadProductos = cantidadProductos;
        }

        public float getPrecioUnidad() {
            return precioUnidad;
        }

        public void setPrecioUnidad(float precioUnidad) {
            this.precioUnidad = precioUnidad;
        }

        public byte getCalidad() {
            return calidad;
        }

        public void setCalidad(byte calidad) {
            this.calidad = calidad;
        }

        public Date getFechaVencimiento() {
            return fechaVencimiento;
        }

        public void setFechaVencimiento(Date fechaVencimiento) {
            this.fechaVencimiento = fechaVencimiento;
        }

        public String getCondicionesAlmacenaje() {
            return condicionesAlmacenaje;
        }

        public void setCondicionesAlmacenaje(String condicionesAlmacenaje) {
            this.condicionesAlmacenaje = condicionesAlmacenaje;
        }

        public char getVentaExtranjero() {
            return ventaExtranjero;
        }

        public void setVentaExtranjero(char ventaExtranjero) {
            this.ventaExtranjero = ventaExtranjero;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public char getVigente() {
            return vigente;
        }

        public void setVigente(char vigente) {
            this.vigente = vigente;
        }

        public void addToProductosPedido() {
            FacesContext context = FacesContext.getCurrentInstance();

//            LoteProductoSeleccionado pr = new LoteProductoSeleccionado();
//            pr.setIdProducto(idProducto);
//            pr.setIdProducto(tProductoFacade.find(BigDecimal.valueOf(idProducto)));
//
//            TUsuario usuario = (TUsuario) Util.getSession().getAttribute("usuario");
//            usuario = tUsuarioFacade.find(usuario.getIdusuario());
//            pr.setIdempresa(usuario.getIdrepresentante().getIdempresa());
//            pr.setIdunidad(tUnidadMedicionFacade.find(TUnidadMedicion.getIdunidad()));
//
//            pr.setCantidadproductos(BigInteger.valueOf(cantidadProductos));
//            pr.setPreciounidad(precioUnidad);
//            pr.setCalidad(BigInteger.valueOf(calidad));
//            pr.setFechavencimiento(fechaVencimiento);
//            pr.setCondicionesalmacenaje(condicionesAlmacenaje);
//            pr.setVentaalextranjero(ventaExtranjero);
//            pr.setDescripcion(descripcion);
//            pr.setVigente('1');
//            try {
//                LoteProductoSeleccionado.add(pr);
//            } catch (Exception e) {
//                context.addMessage(null, new FacesMessage(e.getMessage()));
//            }
        }

//        public void deleteProductoFromPedido(LoteProductoSeleccionado pr) {
//            FacesContext context = FacesContext.getCurrentInstance();
//            try {
//                LoteProductoSeleccionado.remove(pr);
//            } catch (Exception e) {
//                context.addMessage(null, new FacesMessage(e.getMessage()));
//            }
//        }
    }

    /*Método que muestra la cantidad de ofertas en progreso - autor Juan Román*/
    public int LoteOfrecidoEnProgreso() {
        int i = 0;
        for (TLoteOfrecido l : ListarLoteOfrecidosP()) {
            if (!l.getIdproductopedido().getIdpedido().getEstadopedido().equals("Requiere Validación")
                    && !l.getIdproductopedido().getIdpedido().getEstadopedido().equals("Requiere Negociación")
                    && !l.getIdproductopedido().getIdpedido().getEstadopedido().equals("Subastado")
                    && !l.getIdproductopedido().getIdpedido().getEstadopedido().equals("Entregado")
                    && !l.getIdproductopedido().getIdpedido().getEstadopedido().equals("Completado")
                    && !l.getIdproductopedido().getIdpedido().getEstadopedido().equals("Anulado")) {
                i++;
            }
        }
        return i;
    }

    /*Método que muestra la cantidad de ofertas rechazadas - autor Juan Román*/
    public int LoteOfrecidosRechazados() {
        int i = 0;
        for (TLoteOfrecido l : ListarLoteOfrecidosP()) {
            if (l.getIdproductopedido().getIdpedido().getEstadopedido().equals("Anulado")) {
                i++;
            }
        }
        return i; 
    }

    /*Método que muestra la cantidad de ofertas completadas - autor Juan Román*/
    public int LoteOfrecidosTerminados() {
        int i = 0;
        for (TLoteOfrecido l : ListarLoteOfrecidosP()) {
            if (l.getIdproductopedido().getIdpedido().getEstadopedido().equals("Completado")) {
                i++;
            }
        }
        return i;
    }

}
