/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.controller;

import javax.inject.Named;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import sv.com.maipogrande.authent.AutentificacionRemota;
import javax.servlet.http.HttpSession;
import sv.com.maipogrande.entities.TUsuario;
import sv.com.maipogrande.model.TUsuarioFacade;
import sv.com.maipogrande.session.Util;

/**
 *
 * @author Maxi
 */
@ManagedBean

public class LoginController {

    @EJB
    private TUsuarioFacade tUsuarioFacade;

    private String email;
    private String pass;
    
    @EJB
    private AutentificacionRemota ejb;
    
    
    
    public String autenticar() {

        if (ejb.autenticar(email, pass)) {
            HttpSession hs = Util.getSession();            
            hs.setAttribute("usuario", tUsuarioFacade.findUsuarioEmail(email).get(0));
            hs.setAttribute("representante", tUsuarioFacade.findUsuarioEmail(email).get(0).getIdrepresentante());
            hs.setAttribute("idusuario", tUsuarioFacade.findUsuarioEmail(email).get(0).getIdusuario());
            hs.setAttribute("email", tUsuarioFacade.findUsuarioEmail(email).get(0).getEmail());
            hs.setAttribute("nombre", tUsuarioFacade.findUsuarioEmail(email).get(0).getIdrepresentante().getNombre());
            hs.setAttribute("apellido1", tUsuarioFacade.findUsuarioEmail(email).get(0).getIdrepresentante().getAppaterno());
            hs.setAttribute("appellido2", tUsuarioFacade.findUsuarioEmail(email).get(0).getIdrepresentante().getApmaterno());
            hs.setAttribute("rol", tUsuarioFacade.findUsuarioEmail(email).get(0).getIdrepresentante().getRolrepresentante());
            hs.setAttribute("idEmpresa", tUsuarioFacade.findUsuarioEmail(email).get(0).getIdrepresentante().getIdempresa());
            
            
            return "index";
        } else {
            FacesMessage fm = new FacesMessage("Datos Incorrectos");
            FacesContext.getCurrentInstance().addMessage("msg", fm);
            return null;
        }
    }
    
    public String cerrarSesion(){
        HttpSession hs = Util.getSession();
        hs.invalidate();
        return "login";
    }
    
 

    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPass() {
        return pass;
    }
    
    public void setPass(String pass) {
        this.pass = pass;
    }    
    
    public TUsuario obtener(BigDecimal IDU) {
        TUsuario us = this.tUsuarioFacade.find(IDU);
        return us;
    }
    
}
