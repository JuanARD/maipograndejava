/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.controller;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import sv.com.maipogrande.bean.UsuarioBean;
import sv.com.maipogrande.entities.TEmpresa;
import sv.com.maipogrande.entities.TPedido;
import sv.com.maipogrande.entities.TProducto;
import sv.com.maipogrande.entities.TProductoPedido;
import sv.com.maipogrande.entities.TRepresentante;
import sv.com.maipogrande.entities.TOfertaTransporte;
import sv.com.maipogrande.entities.TSubasta;
import sv.com.maipogrande.model.TOfertaTransporteFacade;
import sv.com.maipogrande.model.TSubastaFacade;
import sv.com.maipogrande.entities.TUnidadMedicion;
import sv.com.maipogrande.entities.TUsuario;
import sv.com.maipogrande.model.TPedidoFacade;
import sv.com.maipogrande.model.TProductoFacade;
import sv.com.maipogrande.model.TProductoPedidoFacade;
import sv.com.maipogrande.model.TUnidadMedicionFacade;
import sv.com.maipogrande.model.TUsuarioFacade;
import sv.com.maipogrande.entities.VViajesmeses;
import sv.com.maipogrande.model.VViajesmesesFacade;
import sv.com.maipogrande.session.Util;

/**
 * Controller que contiene métodos relacionados al pedido
 *
 * @author Cristián Esquivel
 */
@Named(value = "pedidoController")
@SessionScoped
public class PedidoController implements Serializable {

    //================================================================================
    // Facades para acceder a las clases de la base de datos
    //================================================================================
    @EJB
    private TUsuarioFacade tUsuarioFacade;

    @EJB
    private VViajesmesesFacade vViajeFacade;

    @EJB
    private TUnidadMedicionFacade tUnidadMedicionFacade;

    @EJB
    private TProductoFacade tProductoFacade;

    @EJB
    private TProductoPedidoFacade tProductoPedidoFacade;

    @EJB
    private TPedidoFacade tPedidoFacade;

    @EJB
    private TOfertaTransporteFacade tOfertaTransporteFacade;

    @EJB
    private TSubastaFacade tSubastaFacade;

    //================================================================================
    // Instancias de clases y variables temporales
    //================================================================================
    private TPedido pedido;

    private VViajesmeses vViajesmeses;

    private TProductoPedido productoPedido;

    private TProducto producto;

    private TUnidadMedicion unidadMedicion;

    private TUsuario usuario;

    private TSubasta subasta;

    private TRepresentante represenante;

    private TOfertaTransporte ofertatransporte;

    //================================================================================
    // Instancias de Listas
    //================================================================================
    private List<ProductoSeleccionado> productosSeleccionados;

    private List<TPedido> lisartpedidousuario;

    private List<TSubasta> listasubasta;

    private List<TOfertaTransporte> listaoferta;

    //================================================================================
    // Instancias de Variables
    //================================================================================
    private boolean usarContacto;

    private boolean usarDireccion;

    private long cantidad;

    //================================================================================
    // Constructor
    //================================================================================
    public PedidoController() {
        pedido = new TPedido();
        productoPedido = new TProductoPedido();
        producto = new TProducto();
        unidadMedicion = new TUnidadMedicion();
        productosSeleccionados = new ArrayList<ProductoSeleccionado>();

        represenante = new TRepresentante();
        //usuario = new TUsuario();
        ofertatransporte = new TOfertaTransporte();
        listasubasta = new ArrayList<TSubasta>();
        listaoferta = new ArrayList<TOfertaTransporte>();
    }

    //================================================================================
    // Clase de producto Seleccionado, Sirve como item para un carrito temporal para los productos del pedido
    // Creado por: Cristián Esquivel
    //================================================================================    
    public class ProductoSeleccionado {

        private int idProducto;
        private String tipoProducto;
        private int idUnidadMedicion;
        private String descripcionUni;
        private long cantidad;

        public ProductoSeleccionado() {

        }

        public int getIdProducto() {
            return idProducto;
        }

        public void setIdProducto(int idProducto) {
            this.idProducto = idProducto;
        }

        public String getTipoProducto() {
            return tipoProducto;
        }

        public void setTipoProducto(String tipoProducto) {
            this.tipoProducto = tipoProducto;
        }

        public int getIdUnidadMedicion() {
            return idUnidadMedicion;
        }

        public void setIdUnidadMedicion(int idUnidadMedicion) {
            this.idUnidadMedicion = idUnidadMedicion;
        }

        public String getDescripcionUni() {
            return descripcionUni;
        }

        public void setDescripcionUni(String descripcionUni) {
            this.descripcionUni = descripcionUni;
        }

        public long getCantidad() {
            return cantidad;
        }

        public void setCantidad(long cantidad) {
            this.cantidad = cantidad;
        }
    }

    //================================================================================
    // Getters y Setters
    //================================================================================    
    public List<ProductoSeleccionado> getProductosSeleccionados() {
        return productosSeleccionados;
    }

    public void setProductosSeleccionados(List<ProductoSeleccionado> productosSeleccionados) {
        this.productosSeleccionados = productosSeleccionados;
    }

    public boolean isUsarContacto() {
        return usarContacto;
    }

    public void setUsarContacto(boolean usarContacto) {
        this.usarContacto = usarContacto;
    }

    public boolean isUsarDireccion() {
        return usarDireccion;
    }

    public void setUsarDireccion(boolean usarDireccion) {
        this.usarDireccion = usarDireccion;
    }

    public long getCantidad() {
        return cantidad;
    }

    public void setCantidad(long cantidad) {
        this.cantidad = cantidad;
    }

    public TUnidadMedicion getUnidadMedicion() {
        return unidadMedicion;
    }

    public void setUnidadMedicion(TUnidadMedicion unidadMedicion) {
        this.unidadMedicion = unidadMedicion;
    }

    public TProducto getProducto() {
        return producto;
    }

    public void setProducto(TProducto producto) {
        this.producto = producto;
    }

    public TProductoPedido getProductoPedido() {
        return productoPedido;
    }

    public void setProductoPedido(TProductoPedido productoPedido) {
        this.productoPedido = productoPedido;
    }

    public TPedido getPedido() {
        return pedido;
    }

    public void setPedido(TPedido pedido) {
        this.pedido = pedido;
    }

    // vista pedidoviaje 
    public TRepresentante getRepresenante() {
        return represenante;
    }

    public void setRepresenante(TRepresentante represenante) {
        this.represenante = represenante;
    }

    public TOfertaTransporte getOfertatransporte() {
        return ofertatransporte;
    }

    public void setOfertatransporte(TOfertaTransporte ofertatransporte) {
        this.ofertatransporte = ofertatransporte;
    }

    public TUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(TUsuario usuario) {
        this.usuario = usuario;
    }

    public TSubasta getSubasta() {
        return subasta;
    }

    public void setSubasta(TSubasta subasta) {
        this.subasta = subasta;
    }

    //================================================================================
    // Métodos de Listar
    //================================================================================    
    public List<TProductoPedido> listarTodosProductoPedidos(TPedido idPed) {
        return tProductoPedidoFacade.findProductoPedidosByIdPedido(idPed);
    }

    public List<TPedido> listarTodosPedidos() {
        return tPedidoFacade.findAll();
    }

    public List<TPedido> listarPedidosPropios() {
        return tPedidoFacade.findPedidoByRepresentante(obtenerUsuario().getIdrepresentante());
    }

    public List<TProductoPedido> listaProductoPedidosRep() {
        return tProductoPedidoFacade.findProductoPedidosByRep(obtenerUsuario().getIdrepresentante());
    }

    public List<TProducto> listarTodosProductos() {
        return tProductoFacade.findAll();
    }

    public List<TUnidadMedicion> listarUnidadesMedicion() {
        return tUnidadMedicionFacade.findAll();
    }

    public List<TPedido> listarPedidosPorRol(String rol) {
        if (rol.equals("Productor")) {
            return listarTodosPedidos();
        } else if (!rol.equals("Productor")) {
            return getLisartpedidousuario();
        } else {
            return null;
        }
    }

    public List<TSubasta> listarTodosSubastaPedido(TPedido idPed) {
        return tSubastaFacade.listarPedidosSubasta(idPed);
    }

    public List<TOfertaTransporte> listarTodosSubastaOferta(TSubasta idsub) {
        return tOfertaTransporteFacade.listarSubastaOferta(idsub);
    }

    public void setLisartpedidousuario(List<TPedido> lisartpedidousuario) {
        this.lisartpedidousuario = lisartpedidousuario;
    }

    public List<TSubasta> getListasubasta() {
        return listasubasta;
    }

    public void setListasubasta(List<TSubasta> listasubasta) {
        this.listasubasta = listasubasta;
    }

    public List<TPedido> getLisartpedidousuario() {
        represenante = (TRepresentante) Util.getSession().getAttribute("representante");
        lisartpedidousuario = tPedidoFacade.listarPedidosRepresentante(represenante);
        return lisartpedidousuario;
    }

    public List<TPedido> listarOfertaTransporte() {
        return tOfertaTransporteFacade.findOfertaByIdRepreVigente(obtenerUsuario().getIdrepresentante(), '1');
    }

    //================================================================================
    // CUD de Pedido
    //================================================================================
    // Método para crear un nuevo pedido, incluyendo los productos que se agregaron al pedido
    // Creado por Cristián Esquivel
    public void addPedido() {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            // Verifica primero si se agregó algún producto al pedido
            if (productosSeleccionados.isEmpty()) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Agregue productos al pedido por favor"));
                return;
            }
            // Verifica si se especificó un fono de contacto
            if (pedido.getFono().trim().isEmpty()) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Especifique Fono de Contacto"));
                return;
            }
            // Verifica si se especificó la dirección de destino
            if (pedido.getDireccion().trim().isEmpty()) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Especifique Dirección"));
                return;
            }
            // Verifica que se especifique algún comentario inicial respecto al pedido
            if (pedido.getObservacioninicial().trim().isEmpty()) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Especifique Observación Inicial"));
                return;
            }
            // Instancia de Clases locales
            TPedido p = new TPedido();
            TProducto pr = new TProducto();
            TUnidadMedicion u = new TUnidadMedicion();

            p.setIdrepresentante(obtenerUsuario().getIdrepresentante()); // Se extrae el usuario iniciado sesión
            p.setRutacontrato("Sin Especificar"); //
            Calendar cal = Calendar.getInstance();
            Date hoy = cal.getTime();
            p.setFechainiciopedido(hoy); // Se especifica que la fecha de inicio del pedido es la fecha actual
            cal.add(Calendar.MONTH, 1);
            Date enUnMesMasEstipulada = cal.getTime(); // Se estipula que la fecha de entrega es al siguiente mes
            p.setFechaentregaestipulada(enUnMesMasEstipulada);
            p.setFechaentregareal(null); //
            p.setObservacioninicial(pedido.getObservacioninicial()); // Se asigna la observación inicial
            p.setEstadopedido("Requiere Validación"); // Se pone el estado cuando un pedido recién se ingresa, que es "Requiere Validación"
            p.setCalidadentrega(""); // La calidad de entrega no se especifica hasta más adelante en el progreso del pedido
            p.setObservacionentrega("No Especificada"); // La Observación de Entrega no se especifica hasta más adelante en el progreso del pedido
            p.setDevuelto('0'); // Si se ha devuelto se no se especifica hasta más adelante en el progreso del pedido 
            p.setVigente('1'); // Se coloca por defecto que está vigente el pedido
            p.setDireccion(pedido.getDireccion()); // Se asigna la dirección de entrega
            p.setFono(pedido.getFono()); // Se asigna el fono de contacto
            tPedidoFacade.create(p); // Se registra el pedido a la base de datos
            for (ProductoSeleccionado ps : productosSeleccionados) { //Por cada producto ingresado al pedido se ingresa un nuevo producto al pedido en la base de datos
                TProductoPedido tpd = new TProductoPedido();
                tpd.setIdpedido(p);
                tpd.setIdproducto(tProductoFacade.find(BigDecimal.valueOf(ps.idProducto))); // Se asigna el producto
                tpd.setIdunidad(tUnidadMedicionFacade.find(BigDecimal.valueOf(ps.idUnidadMedicion))); // Se asigna la unidad de medición
                tpd.setCantidad(BigInteger.valueOf(ps.cantidad)); // Se asigna la cantidad por unidad
                tProductoPedidoFacade.create(tpd); // Se ingresa a la base de datos un producto al pedido individualmente
            }
            context.addMessage(null, new FacesMessage("", "Pedido Ingresado Correctamente"));// Mensaje de que está todo correcto
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
        clearPedido();// Limpia el pedido
    }

    // Método para redireccionar a la siguiente página que muestra el detalle del pedido
    // Creado por Cristián Esquivel
    public String verDetallePedido(TPedido p) {
        this.pedido = p;
        return "verDetallePedido?faces-redirect=true";
    }

//juan Román
    public String verDetalleOfertaPedido(TPedido p) {
        this.pedido = p;
        return "DetalleOfertaPedido?faces-redirect=true";
    }

    // Método para anular el pedido, esto cambia el estado del pedido a "Anulado" y este deja de ser vigente, avisandole por correo al usuario sobre la situación
    // Creado por Cristián Esquivel
    public void anularPedido() throws MessagingException {
        FacesContext context = FacesContext.getCurrentInstance();
        pedido.setEstadopedido("Anulado");
        pedido.setVigente('0');
        tPedidoFacade.edit(pedido);
        Properties prop = new Properties();
        prop.setProperty("mail.smtp.host", "smtp.gmail.com");
        prop.setProperty("mail.smtp.starttls.enable", "true");
        prop.setProperty("mail.smtp.port", "587");
        prop.setProperty("mail.smtp.auth", "true");

        Session sesion = Session.getDefaultInstance(prop);
        String correoEmisor = "maipogrande.feriavirtual@gmail.com";
        String contra = "Maipo123";
        String destinatario = getUsuario().getEmail();
        String asunto = "MAIPO GRANDE - Pedido #" + pedido.getIdpedido().toString() + " Anulado.";
        String mensaje = "Estimado/a " + getUsuario().getIdrepresentante().getNombre() + " " + getUsuario().getIdrepresentante().getAppaterno() + " " + getUsuario().getIdrepresentante().getApmaterno() + "\n"
                + "\n"
                + "Ha confirmado una anulación del pedido #" + pedido.getIdpedido().toString() + ".\n"
                + "\n"
                + "El pedido ya no podrá progresar ya que dejará de estar vigente.\n"
                + "\n"
                + "Puede revisar el estado de sus pedidos en nuestra plataforma, si tiene alguna duda o queja puede contactarnos por los medios de contacto ofrecidos en nuestra página web.\n"
                + "\n"
                + "Saludos.";

        MimeMessage m = new MimeMessage(sesion);
        try {
            m.setFrom(new InternetAddress(correoEmisor));
            m.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
            m.setSubject(asunto);
            m.setText(mensaje);

            try (Transport t = sesion.getTransport("smtp")) {
                t.connect(correoEmisor, contra);
                t.sendMessage(m, m.getRecipients(Message.RecipientType.TO));
            }
        } catch (AddressException ex) {
            Logger.getLogger(UsuarioBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        context.addMessage(null, new FacesMessage("", "Pedido Anulado Correctamente"));
    }

    // Método para finalizar el pedido, esto cambia el estado del pedido a "Completado" y envía un correo al usuario
    // Creado por Cristián Esquivel
    public void completarPedido() throws MessagingException {
        FacesContext context = FacesContext.getCurrentInstance();
        pedido.setEstadopedido("Completado");
        tPedidoFacade.edit(pedido);
        Properties prop = new Properties();
        prop.setProperty("mail.smtp.host", "smtp.gmail.com");
        prop.setProperty("mail.smtp.starttls.enable", "true");
        prop.setProperty("mail.smtp.port", "587");
        prop.setProperty("mail.smtp.auth", "true");

        Session sesion = Session.getDefaultInstance(prop);
        String correoEmisor = "maipogrande.feriavirtual@gmail.com";
        String contra = "Maipo123";
        String destinatario = getUsuario().getEmail();
        String asunto = "MAIPO GRANDE - Pedido #" + pedido.getIdpedido().toString() + " Completado.";
        String mensaje = "Estimado/a " + getUsuario().getIdrepresentante().getNombre() + " " + getUsuario().getIdrepresentante().getAppaterno() + " " + getUsuario().getIdrepresentante().getApmaterno() + "\n"
                + "\n"
                + "Ha confirmado que el pedido #" + pedido.getIdpedido().toString() + " lo ha recibido correctamente y ha aceptado el trato.\n"
                + "Puede revisar el estado de sus pedidos en nuestra plataforma.\n"
                + "\n"
                + "Gracias por preferirnos.\n"
                + "\n"
                + "Saludos.";

        MimeMessage m = new MimeMessage(sesion);
        try {
            m.setFrom(new InternetAddress(correoEmisor));
            m.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
            m.setSubject(asunto);
            m.setText(mensaje);

            try (Transport t = sesion.getTransport("smtp")) {
                t.connect(correoEmisor, contra);
                t.sendMessage(m, m.getRecipients(Message.RecipientType.TO));
            }
        } catch (AddressException ex) {
            Logger.getLogger(UsuarioBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        context.addMessage(null, new FacesMessage("", "Pedido Completado Correctamente"));
    }

    //cambiar estado pedido - transportista - Juan Román
    public void cambiarEstadoEntregado() {
        FacesContext context = FacesContext.getCurrentInstance();
        pedido.setFechaentregareal(Calendar.getInstance().getTime());
        pedido.setEstadopedido("Entregado");
        tPedidoFacade.edit(pedido);
        context.addMessage(null, new FacesMessage("", "Pedido Entregado Correctamente"));
    }

    //cambiar estado pedido - transportista - Juan Román
    public void cambiarEstadoaRecorrido() {
        FacesContext context = FacesContext.getCurrentInstance();
        pedido.setEstadopedido("En Recorrido");
        tPedidoFacade.edit(pedido);
        context.addMessage(null, new FacesMessage("", "Pedido En Recorrido"));
    }
//Ingresar observacion de entrega - transportista - Juan Román
    public String modificarObservacionEntrega() {
        pedido.setObservacionentrega(pedido.getObservacionentrega());
        tPedidoFacade.edit(pedido);
        return "verDetallePedido?faces-redirect=true";
    }

    //================================================================================
    // CD de los productos seleccionados para el pedido
    //================================================================================    
    // Método de añadir un producto al pedido cuando este se está creando
    // Creado por Cristián Esquivel
    public void addToPedido() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (producto == null) { // Verifica si el producto seleccionado no es nulo
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Seleccione Producto"));
            return;
        }
        if (cantidad == 0) { // Verifica si la cantidad no es 0
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "La cantidad debe ser mayor a 0"));
            return;
        }
        if (unidadMedicion == null) { // Verifica si la unidad de medición seleccionada no es nula
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Seleccione Unidad de Medición"));
            return;
        }
        ProductoSeleccionado p = new ProductoSeleccionado();
        p.setIdProducto(Integer.parseInt(producto.getIdproducto().toString())); // Asigna el id del producto
        p.setTipoProducto(producto.getNombre() + " " + producto.getTipo()); // Asigna el nombre y tipo del producto para mostrar en la lista
        p.setIdUnidadMedicion(Integer.parseInt(unidadMedicion.getIdunidad().toString())); // Asigna el id de la unidad de medición
        p.setDescripcionUni(unidadMedicion.getDescripcion()); // Asigna la descripción de la unidad de medición
        p.setCantidad(cantidad); // Asigna la cantidad por unidad
        try {
            productosSeleccionados.add(p); // Se añade el producto a la lista local, aún no ingresada a la base de datos
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
    }

    // Método para eliminar el producto del pedido cuando este se está creando
    // Creado por Cristián Esquivel
    public void deleteProductoFromPedido(ProductoSeleccionado p) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            productosSeleccionados.remove(p);
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
    }

    // Método para limpiar el controller con los datos del pedido, producto, unidad y productos del pedido
    // Creado por Cristián Esquivel
    public void clearPedido() {
        this.pedido = new TPedido();
        this.cantidad = 0;
        this.producto = new TProducto();
        this.productoPedido = new TProductoPedido();
        this.productosSeleccionados = new ArrayList<>();
        this.unidadMedicion = new TUnidadMedicion();
        this.usarContacto = false;
        this.usarDireccion = false;
    }

    // Método que verifica si es que se activó el checkbox específico desde el frontend para asignar el contacto o la dirección de la cuenta al pedido
    // Creado por Cristián Esquivel
    public void verificarCheckBoxes() {
        if (usarContacto) {
            pedido.setFono(obtenerUsuario().getIdrepresentante().getFono1());
        } else {
            pedido.setFono("");
        }
        if (usarDireccion) {
            pedido.setDireccion(obtenerUsuario().getIdrepresentante().getDireccion());
        } else {
            pedido.setDireccion("");
        }
    }

    // Método para extraer el usuario de la sesión
    TUsuario obtenerUsuario() {
        usuario = (TUsuario) Util.getSession().getAttribute("usuario");
        usuario = tUsuarioFacade.find(usuario.getIdusuario());
        return usuario;
    }

    public void leerPedido(TPedido pedSelected) {
        pedido = pedSelected;
    }

    public void confirmarPedido() {

        FacesContext context = FacesContext.getCurrentInstance();
        String id = pedido.getIdpedido().toString();

        if (pedido.getDevuelto() == '0') {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Pedido N°" + id + " - ", "Entrega Confirmada"));
            pedido.setVigente('0');
        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Pedido N°" + id + " - ", "Entrega Rechazada"));
        }
        tPedidoFacade.edit(pedido);
    }

    // Método que devuelve la cantidad de pedidos por validar
    // Creado por Cristián Esquivel
    public int pedidosPorValidar() {
        int i = 0;
        for (TPedido p : listarPedidosPropios()) {
            if (p.getEstadopedido().equals("Requiere Validación")) {
                i++;
            }
        }
        return i;
    }

    // Método que devuelve la cantidad de pedidos en progreso
    // Creado por Cristián Esquivel
    public int pedidosEnProgreso() {
        int i = 0;
        for (TPedido p : listarPedidosPropios()) {
            if (!p.getEstadopedido().equals("Requiere Validación")
                    && !p.getEstadopedido().equals("Anulado")
                    && !p.getEstadopedido().equals("Completado")) {
                i++;
            }
        }
        return i;
    }

    // Método que devuelve la cantidad de pedidos anulados
    // Creado por Cristián Esquivel
    public int pedidosRechazados() {
        int i = 0;
        for (TPedido p : listarPedidosPropios()) {
            if (p.getEstadopedido().equals("Anulado")) {
                i++;
            }
        }
        return i;
    }

    // Método que devuelve la cantidad de pedidos terminados/completados
    // Creado por Cristián Esquivel
    public int pedidosTerminados() {
        int i = 0;
        for (TPedido p : listarPedidosPropios()) {
            if (p.getEstadopedido().equals("Completado")) {
                i++;
            }
        }
        return i;
    }

    //DASHBOARD JP - TRANSPORTISTA.
    public List<VViajesmeses> listarViajesEmpresa() {
//        return tPedidoFacade.findViajesByRepresentante(obtenerUsuario().getIdrepresentante());
        TEmpresa empresa = obtenerUsuario().getIdrepresentante().getIdempresa();
        return vViajeFacade.findByIdEmpresa(empresa);
    }

    //CANTIDAD DE VIAJES EN PROGRESO.
//    public int viajeEnProgreso() {
//        int i = 0;
//        for (TPedido pp : listarViajesEmpresa()) {
//            if (!pp.getEstadopedido().equals("Requiere Validación")
//                    && !pp.getEstadopedido().equals("En Recorrido")
//                    && !pp.getEstadopedido().equals("A Despachar")) {
//                i++;
//            }
//        }
//        return i;
//    }
//
//    //CANTIDAD DE VIAJES RECHAZADOS.
//    public int viajesRechazados() {
//        int i = 0;
//        for (TPedido pp : listarViajesEmpresa()) {
//            if (pp.getEstadopedido().equals("Anulado")) {
//                i++;
//            }
//        }
//        return i;
//    }
//
//    //CANTIDAD DE VIAJES TERMINADOS.
//    public int viajesTerminados() {
//        int i = 0;
//        for (TPedido pp : listarViajesEmpresa()) {
//            if (pp.getEstadopedido().equals("Entregado")) {
//                i++;
//            }
//        }
//        return i;
//    }
}
