/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.controller;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import sv.com.maipogrande.entities.TOfertaTransporte;
import sv.com.maipogrande.entities.TRepresentante;
import sv.com.maipogrande.entities.TSubasta;
import sv.com.maipogrande.entities.TUsuario;
import sv.com.maipogrande.entities.TVehiculo;
import sv.com.maipogrande.model.TOfertaTransporteFacade;
import sv.com.maipogrande.model.TRepresentanteFacade;
import sv.com.maipogrande.model.TSubastaFacade;
import sv.com.maipogrande.model.TUsuarioFacade;
import sv.com.maipogrande.model.TVehiculoFacade;
import sv.com.maipogrande.session.Util;

/**
 *
 * @author Juan Román
 */
@Named(value = "ofertaController")
@SessionScoped
public class OfertaController implements Serializable {

    //Facades para acceder a las clases de la base de datos
    @EJB
    private TUsuarioFacade tUsuarioFacade;

    @EJB
    private TOfertaTransporteFacade tOfertaTransporteFacade;

    @EJB
    private TRepresentanteFacade tRepresentanteFacade;

    @EJB
    private TSubastaFacade tSubastaFacade;

    @EJB
    private TVehiculoFacade tVehiculoFacade;

    /*------------Instancias de clases-----------------*/
    private TOfertaTransporte ofertaTransporte;

    private TRepresentante representante;

    private TSubasta subasta;

    private TVehiculo vehiculo;

    private long costoTransporte;

    private TSubasta idSubasta;

    private TUsuario usuario;

    /*Constructor*/
    public OfertaController() {
        subasta = new TSubasta();
        ofertaTransporte = new TOfertaTransporte();
        representante = new TRepresentante();
        vehiculo = new TVehiculo();
    }

    /*get y set*/
    public long getCostoTransporte() {
        return costoTransporte;
    }

    public void setCostoTransporte(long costoTransporte) {
        this.costoTransporte = costoTransporte;
    }

    public TOfertaTransporte getOfertaTransporte() {
        return ofertaTransporte;
    }

    public void setOfertaTransporte(TOfertaTransporte ofertaTransporte) {
        this.ofertaTransporte = ofertaTransporte;
    }

    public TRepresentante getRepresentante() {
        return representante;
    }

    public void setRepresentante(TRepresentante representante) {
        this.representante = representante;
    }

    public TSubasta getSubasta() {
        return subasta;
    }

    public void setSubasta(TSubasta subasta) {
        this.subasta = subasta;
    }

    public TVehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(TVehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public List<TSubasta> listarSubastas() {
        return tSubastaFacade.findAll();
    }

    /*Métodos de Listar -  - autor Juan Román*/
    public List<TOfertaTransporte> listarOfertaTransporte() {
        return tOfertaTransporteFacade.findProductoPedidosByIdRepre(obtenerUsuario().getIdrepresentante());
    }

    public List<TRepresentante> listarRepresentante() {
        return tRepresentanteFacade.findAll();
    }

    public List<TVehiculo> listarVehiculo() {
        return tVehiculoFacade.findProductoPedidosByIdEmpre(obtenerUsuario().getIdrepresentante().getIdempresa());
    }

    public String addSubasta() {
        tSubastaFacade.create(subasta);
        this.subasta = new TSubasta();
        return "crearSubasta";
    }

    public String prepareEditSubasta(TSubasta s) {
        this.subasta = s;
        return "#";
    }

    public String editarSubasta() {
        this.tSubastaFacade.edit(subasta);
        this.subasta = new TSubasta();
        return "#";
    }

    public void deleteSubasta(TSubasta s) {
        this.tSubastaFacade.remove(s);
    }

    /*Método crear oferta - autor Juan Román*/
    public String addOferta() {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if (costoTransporte == 0) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "La cantidad debe ser mayor a 0"));
                /*validación para que cantidad no sea menor a 0*/
                return null;
            }
            TOfertaTransporte ot = new TOfertaTransporte();

            ot.setIdsubasta(tSubastaFacade.find(BigDecimal.valueOf(Integer.parseInt(subasta.getIdsubasta().toString()))));//se guarda el id de subasta ingresada
            ot.setIdvehiculo(tVehiculoFacade.find(BigDecimal.valueOf(Integer.parseInt(vehiculo.getIdvehiculo().toString()))));//se guarda el id de vehículo seleccionada en combobox
            ot.setIdrepresentante(obtenerUsuario().getIdrepresentante());//se obtiene id de representante del usuario ingresado
            ot.setCostotransporte(BigInteger.valueOf(costoTransporte));//obtiene el costo ingresado 
            ot.setVigente('0');//se cambia vigencia a 0, se modifica al seleccionarlo 
            tOfertaTransporteFacade.create(ot);// se crea la oferta de transporte
            context.addMessage(null, new FacesMessage("", "Oferta Ingresada Correctamente"));//mensaje de confirmación

        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(e.getMessage()));//se envía mensaje - autor Juan Román
        }
        clearOferta();
        return "verOfertas?faces-redirect=true";
    }

    TUsuario obtenerUsuario() {//método de obtener usuario - autor cristian esquivel
        usuario = (TUsuario) Util.getSession().getAttribute("usuario");
        usuario = tUsuarioFacade.find(usuario.getIdusuario());
        return usuario;
    }

     public String eliminarOferta(TOfertaTransporte o) {
        this.tOfertaTransporteFacade.remove(o);
        return "verOfertas?faces-redirect=true";   
    }
    
    public String prepareEditSubastaOferta(TSubasta s) { //método para guardar subasta de tabla ingresada - autor Juan Román
        this.subasta = s;
        return "crearOferta?faces-redirect=true";
    }

    public void clearOferta() {//método que limpia oferta
        this.costoTransporte = 0;
        this.ofertaTransporte = new TOfertaTransporte();
        this.vehiculo = new TVehiculo();
        this.representante = new TRepresentante();
    }

    public String FrigorificoV(char frigo) {//método que muestra si y no en tabla - autor Juan Román
        String fri = "";
        if (frigo == '1') {
            fri = "Si";
        } else {
            fri = "No";
        }
        return fri;
    }

}
