/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.model;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import sv.com.maipogrande.entities.TUnidadMedicion;

/**
 *
 * @author Rotzedust
 */
@Stateless
public class TUnidadMedicionFacade extends AbstractFacade<TUnidadMedicion> {

    @PersistenceContext(unitName = "MaipoGrandeWeb-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TUnidadMedicionFacade() {
        super(TUnidadMedicion.class);
    }
    
}
