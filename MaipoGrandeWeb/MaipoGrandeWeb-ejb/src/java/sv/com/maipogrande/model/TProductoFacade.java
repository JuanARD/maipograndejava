/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.model;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import sv.com.maipogrande.entities.TProducto;

/**
 *
 * @author Rotzedust
 */
@Stateless
public class TProductoFacade extends AbstractFacade<TProducto> {

    @PersistenceContext(unitName = "MaipoGrandeWeb-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TProductoFacade() {
        super(TProducto.class);
    }

//    public List<TProducto> listarFrutas() {
//        String consulta = "select distinct(pr.nombre) from TProducto pr order by pr.nombre asc";
//        //consulta = "select p from t_producto";
//        @SuppressWarnings("unchecked")
//        List<TProducto> productos = this.em.createNativeQuery(consulta).getResultList();
//        em.close();
//        return productos;
//    }
//
//    public List<TProducto> listarfrutasXNombre() {
//        TypedQuery<TProducto> query
//                = em.createQuery("SELECT DISTINCT(c.nombre) FROM TProducto c", TProducto.class);
//        List<TProducto> results = query.getResultList();
//        return results;
//    }

}
