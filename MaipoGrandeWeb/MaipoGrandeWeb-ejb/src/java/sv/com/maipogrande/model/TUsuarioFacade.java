/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.model;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import sv.com.maipogrande.entities.TUsuario;

/**
 *
 * @author Rotzedust
 */
@Stateless
public class TUsuarioFacade extends AbstractFacade<TUsuario> {

    @PersistenceContext(unitName = "MaipoGrandeWeb-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TUsuarioFacade() {
        super(TUsuario.class);
    }
    
    public TUsuario findByEmail(String email){
        TypedQuery<TUsuario> tq = em.createQuery("from TUsuario WHERE Email=?", TUsuario.class);
        TUsuario result = tq.setParameter(1, email).getSingleResult();
        
        return result;
    }
      public List<TUsuario> findUsuarioEmail(String email) {
        Query query = em.createQuery("SELECT u FROM TUsuario u WHERE u.email = :email");
        query.setParameter("email", email);
        List<TUsuario> resultList = query.getResultList();
        return resultList;
    }
    
}
