package sv.com.maipogrande.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TemporalType;
import sv.com.maipogrande.entities.TPedido;
import sv.com.maipogrande.entities.TRepresentante;

@Stateless
public class TPedidoFacade extends AbstractFacade<TPedido> {

    @PersistenceContext(unitName = "MaipoGrandeWeb-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TPedidoFacade() {
        super(TPedido.class);
    }

    public List<TPedido> findPedidoByRepresentante(TRepresentante idrep) {
        Query query = em.createQuery("SELECT t FROM TPedido t WHERE t.idrepresentante = :idrep");
        query.setParameter("idrep", idrep);
        List<TPedido> resultList = query.getResultList();
        return resultList;
    }
    
    public List<TPedido> findViajesByRepresentante(TRepresentante idrep) {
        Query query = em.createQuery("SELECT t FROM TPedido t WHERE t.idrepresentante = :idrep");
        query.setParameter("idrep", idrep);
        List<TPedido> resultList = query.getResultList();
        return resultList;
    }

    public List<TPedido> listarPedidosRepresentante(TRepresentante idrep) {
        Query query = em.createQuery("SELECT t FROM TPedido t WHERE t.idrepresentante = :idrepresentante");
        query.setParameter("idrepresentante", idrep);
        System.out.println("IDREPPPP" + idrep);
        List<TPedido> lst = query.getResultList();
        return lst;
    }
   
    public int findCountPedidos(int idRep, int Mes) {
        StoredProcedureQuery query = em.createStoredProcedureQuery("MostraPedidosCompletosMeses");
               
        query.registerStoredProcedureParameter("mes", int.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("idrep", int.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("reg", int.class, ParameterMode.OUT);

        query.setParameter("mes", Mes);
        query.setParameter("idrep", idRep);
        
        query.execute();

        Integer reg = (Integer) query.getOutputParameterValue("reg");
       
        return reg;
    }
    
    public int findCountViajes(int idRep, int Mes) {
        StoredProcedureQuery query = em.createStoredProcedureQuery("MostrarViajesCompletosMeses");
               
        query.registerStoredProcedureParameter("mes", int.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("idrep", int.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("reg", int.class, ParameterMode.OUT);

        query.setParameter("mes", Mes);
        query.setParameter("idrep", idRep);
        
        query.execute();

        Integer reg = (Integer) query.getOutputParameterValue("reg");
       
        return reg;
    }

}
