/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.model;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import sv.com.maipogrande.entities.TEmpresa;
import sv.com.maipogrande.entities.TLoteProducto;
import sv.com.maipogrande.entities.TProducto;
import sv.com.maipogrande.entities.TRepresentante;

/**
 *
 * @author Rotzedust
 */
@Stateless
public class TLoteProductoFacade extends AbstractFacade<TLoteProducto> {

    @PersistenceContext(unitName = "MaipoGrandeWeb-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TLoteProductoFacade() {
        super(TLoteProducto.class);
    }
    
    //No lo quiero borrar, por si acaso
    public List<TLoteProducto> listarPedidosRepresentante(TEmpresa idemp) {
        Query query = em.createQuery("SELECT t FROM TLoteProducto t WHERE t.idempresa = :idempresa");
        query.setParameter("idempresa", idemp);
        List<TLoteProducto> lst = query.getResultList();
        return lst;
    }
    
    public List<TLoteProducto> listarLotesRepresentante(TEmpresa idemp) {
        Query query = em.createQuery("SELECT t FROM TLoteProducto t WHERE t.idempresa = :idempresa");
        query.setParameter("idempresa", idemp);
        List<TLoteProducto> lst = query.getResultList();
        return lst;
    }
}
