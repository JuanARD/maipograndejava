/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.model;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import sv.com.maipogrande.entities.TOfertaTransporte;
import sv.com.maipogrande.entities.TPedido;
import sv.com.maipogrande.entities.TRepresentante;
import sv.com.maipogrande.entities.TSubasta;

/**
 *
 * @author Rotzedust
 */
@Stateless
public class TOfertaTransporteFacade extends AbstractFacade<TOfertaTransporte> {

    @PersistenceContext(unitName = "MaipoGrandeWeb-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TOfertaTransporteFacade() {
        super(TOfertaTransporte.class);
    }

    public List<TOfertaTransporte> findProductoPedidosByIdRepre(TRepresentante idRep) {
        Query query = em.createQuery("SELECT t FROM TOfertaTransporte t WHERE t.idrepresentante = :idRep");
        query.setParameter("idRep", idRep);
        List<TOfertaTransporte> resultList = query.getResultList();
        return resultList;
    }

     //obtener todo de la tpedido filtrando por id de oferta de transporte y vigencia - Juan Román
    public List<TPedido> findOfertaByIdRepreVigente(TRepresentante idRep, char pr) {
        Query query = em.createQuery("SELECT r FROM TOfertaTransporte t\n"
                + "join t.idsubasta p\n"
                + "join p.idpedido r\n"
                + " WHERE t.idrepresentante = :idRep AND t.vigente = :pr");
        query.setParameter("idRep", idRep);
        query.setParameter("pr", pr);
        List<TPedido> resultList = query.getResultList();
        return resultList;
    }

    public List<TOfertaTransporte> listarSubastaOferta(TSubasta idsub) {
        Query query = em.createQuery("SELECT t FROM TOfertaTransporte t WHERE t.idsubasta = :idsubasta");
        query.setParameter("idsubasta", idsub);
        System.out.println("IDOFERTA" + idsub);
        List<TOfertaTransporte> resultListOferta = query.getResultList();
        //  System.out.println("LISTT"+resultList.get(0).getIdpedido());
        return resultListOferta;
    }

}
