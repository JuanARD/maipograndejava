/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.model;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import sv.com.maipogrande.entities.TPedido;
import sv.com.maipogrande.entities.TProductoPedido;
import sv.com.maipogrande.entities.TRepresentante;

/**
 *
 * @author Rotzedust
 */
@Stateless
public class TProductoPedidoFacade extends AbstractFacade<TProductoPedido> {

    @PersistenceContext(unitName = "MaipoGrandeWeb-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TProductoPedidoFacade() {
        super(TProductoPedido.class);
    }

    public List<TProductoPedido> findProductoPedidosByIdPedido(TPedido idped) {
        Query query = em.createQuery("SELECT t FROM TProductoPedido t WHERE t.idpedido = :idped");
        query.setParameter("idped", idped);
        List<TProductoPedido> resultList = query.getResultList();
        return resultList;
    }

    public List<TProductoPedido> findProductoPedidosByRep(TRepresentante idrep) {
        Query query = em.createQuery("Select t from TProductoPedido t\n"
                + "join t.idpedido p\n"
                + "join p.idrepresentante r\n"
                + "where r.idrepresentante=:idrep");
        query.setParameter("idrep", idrep.getIdrepresentante());
        List<TProductoPedido> resultList = query.getResultList();
        return resultList;
    }
    
    public List<Object[]> findProductosMasPedidosMeses(int meses) {
        
        StoredProcedureQuery query = em.createStoredProcedureQuery("ListarProductosVendidosMeses");

        query.registerStoredProcedureParameter("mesesAtras", int.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("reg", void.class, ParameterMode.REF_CURSOR);
        query.setParameter("mesesAtras", meses);

        query.execute();
        List<Object[]> o = query.getResultList();

        return o;
    }
}
