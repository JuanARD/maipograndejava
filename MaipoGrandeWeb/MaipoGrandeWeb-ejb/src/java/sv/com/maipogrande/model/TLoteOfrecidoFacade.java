/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.model;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import sv.com.maipogrande.entities.TLoteOfrecido;
import sv.com.maipogrande.entities.TRepresentante;

/**
 *
 * @author Rotzedust
 */
@Stateless
public class TLoteOfrecidoFacade extends AbstractFacade<TLoteOfrecido> {

    @PersistenceContext(unitName = "MaipoGrandeWeb-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TLoteOfrecidoFacade() {
        super(TLoteOfrecido.class);
    }
 
    public List<TLoteOfrecido> findLoteOfrecidoByRepresentante(TRepresentante idrep) {
        Query query = em.createQuery("SELECT t FROM TLoteOfrecido t WHERE t.idrepresentante = :idrep");
        query.setParameter("idrep", idrep);
        List<TLoteOfrecido> resultList = query.getResultList();
        return resultList;
    }       
    
}
