/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.model;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import sv.com.maipogrande.entities.TEmpresa;

/**
 *
 * @author Rotzedust
 */
@Stateless
public class TEmpresaFacade extends AbstractFacade<TEmpresa> {

    @PersistenceContext(unitName = "MaipoGrandeWeb-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TEmpresaFacade() {
        super(TEmpresa.class);
    }
    
}
