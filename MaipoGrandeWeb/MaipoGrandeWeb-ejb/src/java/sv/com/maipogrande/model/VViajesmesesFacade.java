/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.model;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import sv.com.maipogrande.entities.TEmpresa;
import sv.com.maipogrande.entities.TVehiculo;
import sv.com.maipogrande.entities.VViajesmeses;

/**
 *
 * @author Juan Hernandez
 */
@Stateless
public class VViajesmesesFacade extends AbstractFacade<VViajesmeses> {

    @PersistenceContext(unitName = "MaipoGrandeWeb-ejbPU")
    private EntityManager em;

    public VViajesmesesFacade(Class<VViajesmeses> entityClass) {
        super(entityClass);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VViajesmesesFacade() {
        super(VViajesmeses.class);
    }
  
    public List<VViajesmeses> findByIdEmpresa(TEmpresa idEmp){
        Query query = em.createQuery("SELECT v FROM VViajesmeses v WHERE v.id_empresa = :idEmp");
        query.setParameter("idEmp", idEmp);
        List<VViajesmeses> resultList = query.getResultList();
        return resultList;
    }

}
