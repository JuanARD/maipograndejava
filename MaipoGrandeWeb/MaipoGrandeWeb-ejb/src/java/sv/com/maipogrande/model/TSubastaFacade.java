
package sv.com.maipogrande.model;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import sv.com.maipogrande.entities.TPedido;
import sv.com.maipogrande.entities.TSubasta;

@Stateless
public class TSubastaFacade extends AbstractFacade<TSubasta> {

    @PersistenceContext(unitName = "MaipoGrandeWeb-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TSubastaFacade() {
        super(TSubasta.class);
    }

    public List<TSubasta> obtenerSubastas(String cadena) {
        Query q = em.createNativeQuery("SELECT idsubasta, nombresubasta, idpedido, fechapublicacion, fechatermino, idrepresentante FROM t_subasta where idsubasta = " + cadena + "", TSubasta.class);
        System.out.println("cadena" + cadena);
        List<TSubasta> lst = q.getResultList();
        return lst;
    }

    public List<TSubasta> listarPedidosSubasta(TPedido idped) {
        Query query = em.createQuery("SELECT t FROM TSubasta t WHERE t.idpedido = :idpedido");
        query.setParameter("idpedido", idped);
        System.out.println("IDSUBBB" + idped);
        List<TSubasta> resultListPedido = query.getResultList();
        return resultListPedido;
    }
}
