/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.model;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import sv.com.maipogrande.entities.TEmpresa;
import sv.com.maipogrande.entities.TVehiculo;

/**
 *
 * @author Rotzedust
 */
@Stateless
public class TVehiculoFacade extends AbstractFacade<TVehiculo> {

    @PersistenceContext(unitName = "MaipoGrandeWeb-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TVehiculoFacade() {
        super(TVehiculo.class);
    }

    public List<TVehiculo> findProductoPedidosByIdEmpre(TEmpresa idEmp) {
        Query query = em.createQuery("SELECT t FROM TVehiculo t WHERE t.idempresa = :idEmp");
        query.setParameter("idEmp", idEmp);
        List<TVehiculo> resultList = query.getResultList();
        return resultList;
    }
        
        public List<TVehiculo> filtrarVehiculo(TEmpresa idEmp) {
        Query query = em.createQuery("SELECT t FROM TVehiculo t WHERE t.idempresa = :idEmp");
        query.setParameter("idEmp", idEmp);
        List<TVehiculo> resultList = query.getResultList();
        return resultList;
    }


}
