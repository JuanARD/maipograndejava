/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rotzedust
 */
@Entity
@Table(name = "T_EMPRESA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TEmpresa.findAll", query = "SELECT t FROM TEmpresa t")
    , @NamedQuery(name = "TEmpresa.findByIdempresa", query = "SELECT t FROM TEmpresa t WHERE t.idempresa = :idempresa")
    , @NamedQuery(name = "TEmpresa.findByRutempresa", query = "SELECT t FROM TEmpresa t WHERE t.rutempresa = :rutempresa")
    , @NamedQuery(name = "TEmpresa.findByTipoempresa", query = "SELECT t FROM TEmpresa t WHERE t.tipoempresa = :tipoempresa")
    , @NamedQuery(name = "TEmpresa.findByNombrecomercial", query = "SELECT t FROM TEmpresa t WHERE t.nombrecomercial = :nombrecomercial")
    , @NamedQuery(name = "TEmpresa.findByRazonsocial", query = "SELECT t FROM TEmpresa t WHERE t.razonsocial = :razonsocial")
    , @NamedQuery(name = "TEmpresa.findByDireccion", query = "SELECT t FROM TEmpresa t WHERE t.direccion = :direccion")
    , @NamedQuery(name = "TEmpresa.findByCiudad", query = "SELECT t FROM TEmpresa t WHERE t.ciudad = :ciudad")
    , @NamedQuery(name = "TEmpresa.findByPais", query = "SELECT t FROM TEmpresa t WHERE t.pais = :pais")
    , @NamedQuery(name = "TEmpresa.findByFono1", query = "SELECT t FROM TEmpresa t WHERE t.fono1 = :fono1")
    , @NamedQuery(name = "TEmpresa.findByFono2", query = "SELECT t FROM TEmpresa t WHERE t.fono2 = :fono2")
    , @NamedQuery(name = "TEmpresa.findByEmail", query = "SELECT t FROM TEmpresa t WHERE t.email = :email")
    , @NamedQuery(name = "TEmpresa.findByWeb", query = "SELECT t FROM TEmpresa t WHERE t.web = :web")})
public class TEmpresa implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDEMPRESA")
    private BigDecimal idempresa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "RUTEMPRESA")
    private String rutempresa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "TIPOEMPRESA")
    private String tipoempresa;
    @Size(max = 50)
    @Column(name = "NOMBRECOMERCIAL")
    private String nombrecomercial;
    @Size(max = 50)
    @Column(name = "RAZONSOCIAL")
    private String razonsocial;
    @Lob
    @Column(name = "LOGO")
    private Serializable logo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "DIRECCION")
    private String direccion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CIUDAD")
    private String ciudad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "PAIS")
    private String pais;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FONO1")
    private BigInteger fono1;
    @Column(name = "FONO2")
    private BigInteger fono2;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "EMAIL")
    private String email;
    @Size(max = 60)
    @Column(name = "WEB")
    private String web;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idempresa")
    private List<TVehiculo> tVehiculoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idempresa")
    private List<TRepresentante> tRepresentanteList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idempresa")
    private List<TLoteProducto> tLoteProductoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idempresa")
    private List<TContrato> tContratoList;

    public TEmpresa() {
    }

    public TEmpresa(BigDecimal idempresa) {
        this.idempresa = idempresa;
    }

    public TEmpresa(BigDecimal idempresa, String rutempresa, String tipoempresa, String direccion, String ciudad, String pais, BigInteger fono1, String email) {
        this.idempresa = idempresa;
        this.rutempresa = rutempresa;
        this.tipoempresa = tipoempresa;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.pais = pais;
        this.fono1 = fono1;
        this.email = email;
    }

    public BigDecimal getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(BigDecimal idempresa) {
        this.idempresa = idempresa;
    }

    public String getRutempresa() {
        return rutempresa;
    }

    public void setRutempresa(String rutempresa) {
        this.rutempresa = rutempresa;
    }

    public String getTipoempresa() {
        return tipoempresa;
    }

    public void setTipoempresa(String tipoempresa) {
        this.tipoempresa = tipoempresa;
    }

    public String getNombrecomercial() {
        return nombrecomercial;
    }

    public void setNombrecomercial(String nombrecomercial) {
        this.nombrecomercial = nombrecomercial;
    }

    public String getRazonsocial() {
        return razonsocial;
    }

    public void setRazonsocial(String razonsocial) {
        this.razonsocial = razonsocial;
    }

    public Serializable getLogo() {
        return logo;
    }

    public void setLogo(Serializable logo) {
        this.logo = logo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public BigInteger getFono1() {
        return fono1;
    }

    public void setFono1(BigInteger fono1) {
        this.fono1 = fono1;
    }

    public BigInteger getFono2() {
        return fono2;
    }

    public void setFono2(BigInteger fono2) {
        this.fono2 = fono2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    @XmlTransient
    public List<TVehiculo> getTVehiculoList() {
        return tVehiculoList;
    }

    public void setTVehiculoList(List<TVehiculo> tVehiculoList) {
        this.tVehiculoList = tVehiculoList;
    }

    @XmlTransient
    public List<TRepresentante> getTRepresentanteList() {
        return tRepresentanteList;
    }

    public void setTRepresentanteList(List<TRepresentante> tRepresentanteList) {
        this.tRepresentanteList = tRepresentanteList;
    }

    @XmlTransient
    public List<TLoteProducto> getTLoteProductoList() {
        return tLoteProductoList;
    }

    public void setTLoteProductoList(List<TLoteProducto> tLoteProductoList) {
        this.tLoteProductoList = tLoteProductoList;
    }

    @XmlTransient
    public List<TContrato> getTContratoList() {
        return tContratoList;
    }

    public void setTContratoList(List<TContrato> tContratoList) {
        this.tContratoList = tContratoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idempresa != null ? idempresa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TEmpresa)) {
            return false;
        }
        TEmpresa other = (TEmpresa) object;
        if ((this.idempresa == null && other.idempresa != null) || (this.idempresa != null && !this.idempresa.equals(other.idempresa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.maipogrande.entities.TEmpresa[ idempresa=" + idempresa + " ]";
    }
    
}
