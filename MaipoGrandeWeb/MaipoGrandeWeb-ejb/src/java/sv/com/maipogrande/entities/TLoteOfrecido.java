/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rotzedust
 */
@Entity
@Table(name = "T_LOTE_OFRECIDO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TLoteOfrecido.findAll", query = "SELECT t FROM TLoteOfrecido t")
    , @NamedQuery(name = "TLoteOfrecido.findByIdloteofrecido", query = "SELECT t FROM TLoteOfrecido t WHERE t.idloteofrecido = :idloteofrecido")
    , @NamedQuery(name = "TLoteOfrecido.findByComentario", query = "SELECT t FROM TLoteOfrecido t WHERE t.comentario = :comentario")
    , @NamedQuery(name = "TLoteOfrecido.findByEnbodega", query = "SELECT t FROM TLoteOfrecido t WHERE t.enbodega = :enbodega")
    , @NamedQuery(name = "TLoteOfrecido.findByCantidadsaldo", query = "SELECT t FROM TLoteOfrecido t WHERE t.cantidadsaldo = :cantidadsaldo")
    , @NamedQuery(name = "TLoteOfrecido.findByVigente", query = "SELECT t FROM TLoteOfrecido t WHERE t.vigente = :vigente")})
public class TLoteOfrecido implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDLOTEOFRECIDO")
    private BigDecimal idloteofrecido;
    @Size(max = 300)
    @Column(name = "COMENTARIO")
    private String comentario;
    @Column(name = "ENBODEGA")
    private Character enbodega;
    @Column(name = "CANTIDADSALDO")
    private BigInteger cantidadsaldo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VIGENTE")
    private Character vigente;
    @JoinColumn(name = "IDLOTEPRODUCTO", referencedColumnName = "IDLOTEPRODUCTO")
    @ManyToOne(optional = false)
    private TLoteProducto idloteproducto;
    @JoinColumn(name = "IDPRODUCTOPEDIDO", referencedColumnName = "IDPRODUCTOPEDIDO")
    @ManyToOne(optional = false)
    private TProductoPedido idproductopedido;
    @JoinColumn(name = "IDREPRESENTANTE", referencedColumnName = "IDREPRESENTANTE")
    @ManyToOne
    private TRepresentante idrepresentante;

    public TLoteOfrecido() {
    }

    public TLoteOfrecido(BigDecimal idloteofrecido) {
        this.idloteofrecido = idloteofrecido;
    }

    public TLoteOfrecido(BigDecimal idloteofrecido, Character vigente) {
        this.idloteofrecido = idloteofrecido;
        this.vigente = vigente;
    }

    public BigDecimal getIdloteofrecido() {
        return idloteofrecido;
    }

    public void setIdloteofrecido(BigDecimal idloteofrecido) {
        this.idloteofrecido = idloteofrecido;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Character getEnbodega() {
        return enbodega;
    }

    public void setEnbodega(Character enbodega) {
        this.enbodega = enbodega;
    }

    public BigInteger getCantidadsaldo() {
        return cantidadsaldo;
    }

    public void setCantidadsaldo(BigInteger cantidadsaldo) {
        this.cantidadsaldo = cantidadsaldo;
    }

    public Character getVigente() {
        return vigente;
    }

    public void setVigente(Character vigente) {
        this.vigente = vigente;
    }

    public TLoteProducto getIdloteproducto() {
        return idloteproducto;
    }

    public void setIdloteproducto(TLoteProducto idloteproducto) {
        this.idloteproducto = idloteproducto;
    }

    public TProductoPedido getIdproductopedido() {
        return idproductopedido;
    }

    public void setIdproductopedido(TProductoPedido idproductopedido) {
        this.idproductopedido = idproductopedido;
    }

    public TRepresentante getIdrepresentante() {
        return idrepresentante;
    }

    public void setIdrepresentante(TRepresentante idrepresentante) {
        this.idrepresentante = idrepresentante;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idloteofrecido != null ? idloteofrecido.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TLoteOfrecido)) {
            return false;
        }
        TLoteOfrecido other = (TLoteOfrecido) object;
        if ((this.idloteofrecido == null && other.idloteofrecido != null) || (this.idloteofrecido != null && !this.idloteofrecido.equals(other.idloteofrecido))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.maipogrande.entities.TLoteOfrecido[ idloteofrecido=" + idloteofrecido + " ]";
    }
    
}
