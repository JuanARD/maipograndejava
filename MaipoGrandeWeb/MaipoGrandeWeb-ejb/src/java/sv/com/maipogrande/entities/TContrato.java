/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rotzedust
 */
@Entity
@Table(name = "T_CONTRATO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TContrato.findAll", query = "SELECT t FROM TContrato t")
    , @NamedQuery(name = "TContrato.findByIdcontrato", query = "SELECT t FROM TContrato t WHERE t.idcontrato = :idcontrato")
    , @NamedQuery(name = "TContrato.findByFechainicio", query = "SELECT t FROM TContrato t WHERE t.fechainicio = :fechainicio")
    , @NamedQuery(name = "TContrato.findByFechatermino", query = "SELECT t FROM TContrato t WHERE t.fechatermino = :fechatermino")
    , @NamedQuery(name = "TContrato.findByRutadocumento", query = "SELECT t FROM TContrato t WHERE t.rutadocumento = :rutadocumento")
    , @NamedQuery(name = "TContrato.findByVigente", query = "SELECT t FROM TContrato t WHERE t.vigente = :vigente")})
public class TContrato implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDCONTRATO")
    private BigDecimal idcontrato;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHAINICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechainicio;
    @Column(name = "FECHATERMINO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechatermino;
    @Size(max = 100)
    @Column(name = "RUTADOCUMENTO")
    private String rutadocumento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VIGENTE")
    private Character vigente;
    @JoinColumn(name = "IDEMPRESA", referencedColumnName = "IDEMPRESA")
    @ManyToOne(optional = false)
    private TEmpresa idempresa;

    public TContrato() {
    }

    public TContrato(BigDecimal idcontrato) {
        this.idcontrato = idcontrato;
    }

    public TContrato(BigDecimal idcontrato, Date fechainicio, Character vigente) {
        this.idcontrato = idcontrato;
        this.fechainicio = fechainicio;
        this.vigente = vigente;
    }

    public BigDecimal getIdcontrato() {
        return idcontrato;
    }

    public void setIdcontrato(BigDecimal idcontrato) {
        this.idcontrato = idcontrato;
    }

    public Date getFechainicio() {
        return fechainicio;
    }

    public void setFechainicio(Date fechainicio) {
        this.fechainicio = fechainicio;
    }

    public Date getFechatermino() {
        return fechatermino;
    }

    public void setFechatermino(Date fechatermino) {
        this.fechatermino = fechatermino;
    }

    public String getRutadocumento() {
        return rutadocumento;
    }

    public void setRutadocumento(String rutadocumento) {
        this.rutadocumento = rutadocumento;
    }

    public Character getVigente() {
        return vigente;
    }

    public void setVigente(Character vigente) {
        this.vigente = vigente;
    }

    public TEmpresa getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(TEmpresa idempresa) {
        this.idempresa = idempresa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcontrato != null ? idcontrato.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TContrato)) {
            return false;
        }
        TContrato other = (TContrato) object;
        if ((this.idcontrato == null && other.idcontrato != null) || (this.idcontrato != null && !this.idcontrato.equals(other.idcontrato))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.maipogrande.entities.TContrato[ idcontrato=" + idcontrato + " ]";
    }
    
}
