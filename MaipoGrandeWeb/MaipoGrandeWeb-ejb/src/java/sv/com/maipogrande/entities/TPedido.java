/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rotzedust
 */
@Entity
@Table(name = "T_PEDIDO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TPedido.findAll", query = "SELECT t FROM TPedido t")
    , @NamedQuery(name = "TPedido.findByIdpedido", query = "SELECT t FROM TPedido t WHERE t.idpedido = :idpedido")
    , @NamedQuery(name = "TPedido.findByRutacontrato", query = "SELECT t FROM TPedido t WHERE t.rutacontrato = :rutacontrato")
    , @NamedQuery(name = "TPedido.findByFechainiciopedido", query = "SELECT t FROM TPedido t WHERE t.fechainiciopedido = :fechainiciopedido")
    , @NamedQuery(name = "TPedido.findByFechaentregaestipulada", query = "SELECT t FROM TPedido t WHERE t.fechaentregaestipulada = :fechaentregaestipulada")
    , @NamedQuery(name = "TPedido.findByFechaentregareal", query = "SELECT t FROM TPedido t WHERE t.fechaentregareal = :fechaentregareal")
    , @NamedQuery(name = "TPedido.findByEstadopedido", query = "SELECT t FROM TPedido t WHERE t.estadopedido = :estadopedido")
    , @NamedQuery(name = "TPedido.findByCalidadentrega", query = "SELECT t FROM TPedido t WHERE t.calidadentrega = :calidadentrega")
    , @NamedQuery(name = "TPedido.findByDevuelto", query = "SELECT t FROM TPedido t WHERE t.devuelto = :devuelto")
    , @NamedQuery(name = "TPedido.findByVigente", query = "SELECT t FROM TPedido t WHERE t.vigente = :vigente")
    , @NamedQuery(name = "TPedido.findByDireccion", query = "SELECT t FROM TPedido t WHERE t.direccion = :direccion")
    , @NamedQuery(name = "TPedido.findByFono", query = "SELECT t FROM TPedido t WHERE t.fono = :fono")})
public class TPedido implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDPEDIDO")
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "SEQ_PEDIDO")
    @SequenceGenerator(name="SEQ_PEDIDO",sequenceName = "SEQ_PEDIDO",allocationSize = 1)
    private BigDecimal idpedido;
    @Size(max = 100)
    @Column(name = "RUTACONTRATO")
    private String rutacontrato;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHAINICIOPEDIDO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechainiciopedido;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHAENTREGAESTIPULADA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaentregaestipulada;
    @Column(name = "FECHAENTREGAREAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaentregareal;
    @Lob
    @Column(name = "OBSERVACIONINICIAL")
    private String observacioninicial;
    @Size(max = 30)
    @Column(name = "ESTADOPEDIDO")
    private String estadopedido;
    @Size(max = 15)
    @Column(name = "CALIDADENTREGA")
    private String calidadentrega;
    @Lob
    @Column(name = "OBSERVACIONENTREGA")
    private String observacionentrega;
    @Column(name = "DEVUELTO")
    private Character devuelto;
    @Column(name = "VIGENTE")
    private Character vigente;
    @Size(max = 60)
    @Column(name = "DIRECCION")
    private String direccion;
    @Size(max = 15)
    @Column(name = "FONO")
    private String fono;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idpedido")
    private List<TSubasta> tSubastaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idpedido",fetch=FetchType.LAZY )
    private List<TProductoPedido> tProductoPedidoList;
    @JoinColumn(name = "IDREPRESENTANTE", referencedColumnName = "IDREPRESENTANTE")
    @ManyToOne(optional = false)
    private TRepresentante idrepresentante;

    public TPedido() {
        tProductoPedidoList = new ArrayList<>();
        tSubastaList = new ArrayList<>();
    }

    public TPedido(BigDecimal idpedido) {
        this.idpedido = idpedido;
    }

    public TPedido(BigDecimal idpedido, Date fechainiciopedido, Date fechaentregaestipulada) {
        this.idpedido = idpedido;
        this.fechainiciopedido = fechainiciopedido;
        this.fechaentregaestipulada = fechaentregaestipulada;
    }

    public BigDecimal getIdpedido() {
        return idpedido;
    }

    public void setIdpedido(BigDecimal idpedido) {
        this.idpedido = idpedido;
    }

    public String getRutacontrato() {
        return rutacontrato;
    }

    public void setRutacontrato(String rutacontrato) {
        this.rutacontrato = rutacontrato;
    }

    public Date getFechainiciopedido() {
        return fechainiciopedido;
    }

    public void setFechainiciopedido(Date fechainiciopedido) {
        this.fechainiciopedido = fechainiciopedido;
    }

    public Date getFechaentregaestipulada() {
        return fechaentregaestipulada;
    }

    public void setFechaentregaestipulada(Date fechaentregaestipulada) {
        this.fechaentregaestipulada = fechaentregaestipulada;
    }

    public Date getFechaentregareal() {
        return fechaentregareal;
    }

    public void setFechaentregareal(Date fechaentregareal) {
        this.fechaentregareal = fechaentregareal;
    }

    public String getObservacioninicial() {
        return observacioninicial;
    }

    public void setObservacioninicial(String observacioninicial) {
        this.observacioninicial = observacioninicial;
    }

    public String getEstadopedido() {
        return estadopedido;
    }

    public void setEstadopedido(String estadopedido) {
        this.estadopedido = estadopedido;
    }

    public String getCalidadentrega() {
        return calidadentrega;
    }

    public void setCalidadentrega(String calidadentrega) {
        this.calidadentrega = calidadentrega;
    }

    public String getObservacionentrega() {
        return observacionentrega;
    }

    public void setObservacionentrega(String observacionentrega) {
        this.observacionentrega = observacionentrega;
    }

    public Character getDevuelto() {
        return devuelto;
    }

    public void setDevuelto(Character devuelto) {
        this.devuelto = devuelto;
    }

    public Character getVigente() {
        return vigente;
    }

    public void setVigente(Character vigente) {
        this.vigente = vigente;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFono() {
        return fono;
    }

    public void setFono(String fono) {
        this.fono = fono;
    }

    @XmlTransient
    public List<TSubasta> getTSubastaList() {
        return tSubastaList;
    }

    public void setTSubastaList(List<TSubasta> tSubastaList) {
        this.tSubastaList = tSubastaList;
    }

    @XmlTransient
    public List<TProductoPedido> getTProductoPedidoList() {
        return tProductoPedidoList;
    }

    public void setTProductoPedidoList(List<TProductoPedido> tProductoPedidoList) {
        this.tProductoPedidoList = tProductoPedidoList;
    }

    public TRepresentante getIdrepresentante() {
        return idrepresentante;
    }

    public void setIdrepresentante(TRepresentante idrepresentante) {
        this.idrepresentante = idrepresentante;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpedido != null ? idpedido.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TPedido)) {
            return false;
        }
        TPedido other = (TPedido) object;
        if ((this.idpedido == null && other.idpedido != null) || (this.idpedido != null && !this.idpedido.equals(other.idpedido))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.maipogrande.entities.TPedido[ idpedido=" + idpedido + " ]";
    }
    
}
