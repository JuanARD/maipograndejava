/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rotzedust
 */
@Entity
@Table(name = "T_SUBASTA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TSubasta.findAll", query = "SELECT t FROM TSubasta t")
    , @NamedQuery(name = "TSubasta.findByIdsubasta", query = "SELECT t FROM TSubasta t WHERE t.idsubasta = :idsubasta")
    , @NamedQuery(name = "TSubasta.findByNombresubasta", query = "SELECT t FROM TSubasta t WHERE t.nombresubasta = :nombresubasta")
    , @NamedQuery(name = "TSubasta.findByFechapublicacion", query = "SELECT t FROM TSubasta t WHERE t.fechapublicacion = :fechapublicacion")
    , @NamedQuery(name = "TSubasta.findByFechatermino", query = "SELECT t FROM TSubasta t WHERE t.fechatermino = :fechatermino")})
public class TSubasta implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDSUBASTA")
    private BigDecimal idsubasta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "NOMBRESUBASTA")
    private String nombresubasta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHAPUBLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechapublicacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHATERMINO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechatermino;
    @JoinColumn(name = "IDPEDIDO", referencedColumnName = "IDPEDIDO")
    @ManyToOne(optional = false)
    private TPedido idpedido;
    @JoinColumn(name = "IDREPRESENTANTE", referencedColumnName = "IDREPRESENTANTE")
    @ManyToOne(optional = false)
    private TRepresentante idrepresentante;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idsubasta")
    private List<TOfertaTransporte> tOfertaTransporteList;

    public TSubasta() {
    }

    public TSubasta(BigDecimal idsubasta) {
        this.idsubasta = idsubasta;
    }

    public TSubasta(BigDecimal idsubasta, String nombresubasta, Date fechapublicacion, Date fechatermino) {
        this.idsubasta = idsubasta;
        this.nombresubasta = nombresubasta;
        this.fechapublicacion = fechapublicacion;
        this.fechatermino = fechatermino;
    }

    public BigDecimal getIdsubasta() {
        return idsubasta;
    }

    public void setIdsubasta(BigDecimal idsubasta) {
        this.idsubasta = idsubasta;
    }

    public String getNombresubasta() {
        return nombresubasta;
    }

    public void setNombresubasta(String nombresubasta) {
        this.nombresubasta = nombresubasta;
    }

    public Date getFechapublicacion() {
        return fechapublicacion;
    }

    public void setFechapublicacion(Date fechapublicacion) {
        this.fechapublicacion = fechapublicacion;
    }

    public Date getFechatermino() {
        return fechatermino;
    }

    public void setFechatermino(Date fechatermino) {
        this.fechatermino = fechatermino;
    }

    public TPedido getIdpedido() {
        return idpedido;
    }

    public void setIdpedido(TPedido idpedido) {
        this.idpedido = idpedido;
    }

    public TRepresentante getIdrepresentante() {
        return idrepresentante;
    }

    public void setIdrepresentante(TRepresentante idrepresentante) {
        this.idrepresentante = idrepresentante;
    }

    @XmlTransient
    public List<TOfertaTransporte> getTOfertaTransporteList() {
        return tOfertaTransporteList;
    }

    public void setTOfertaTransporteList(List<TOfertaTransporte> tOfertaTransporteList) {
        this.tOfertaTransporteList = tOfertaTransporteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idsubasta != null ? idsubasta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TSubasta)) {
            return false;
        }
        TSubasta other = (TSubasta) object;
        if ((this.idsubasta == null && other.idsubasta != null) || (this.idsubasta != null && !this.idsubasta.equals(other.idsubasta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.maipogrande.entities.TSubasta[ idsubasta=" + idsubasta + " ]";
    }
    
}
