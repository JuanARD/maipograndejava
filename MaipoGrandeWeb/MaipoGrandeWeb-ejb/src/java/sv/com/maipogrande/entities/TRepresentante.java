/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rotzedust
 */
@Entity
@Table(name = "T_REPRESENTANTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TRepresentante.findAll", query = "SELECT t FROM TRepresentante t")
    , @NamedQuery(name = "TRepresentante.findByIdrepresentante", query = "SELECT t FROM TRepresentante t WHERE t.idrepresentante = :idrepresentante")
    , @NamedQuery(name = "TRepresentante.findByRutrepresentante", query = "SELECT t FROM TRepresentante t WHERE t.rutrepresentante = :rutrepresentante")
    , @NamedQuery(name = "TRepresentante.findByRolrepresentante", query = "SELECT t FROM TRepresentante t WHERE t.rolrepresentante = :rolrepresentante")
    , @NamedQuery(name = "TRepresentante.findByNombre", query = "SELECT t FROM TRepresentante t WHERE t.nombre = :nombre")
    , @NamedQuery(name = "TRepresentante.findByAppaterno", query = "SELECT t FROM TRepresentante t WHERE t.appaterno = :appaterno")
    , @NamedQuery(name = "TRepresentante.findByApmaterno", query = "SELECT t FROM TRepresentante t WHERE t.apmaterno = :apmaterno")
    , @NamedQuery(name = "TRepresentante.findBySexo", query = "SELECT t FROM TRepresentante t WHERE t.sexo = :sexo")
    , @NamedQuery(name = "TRepresentante.findByFechanacimiento", query = "SELECT t FROM TRepresentante t WHERE t.fechanacimiento = :fechanacimiento")
    , @NamedQuery(name = "TRepresentante.findByFono1", query = "SELECT t FROM TRepresentante t WHERE t.fono1 = :fono1")
    , @NamedQuery(name = "TRepresentante.findByFono2", query = "SELECT t FROM TRepresentante t WHERE t.fono2 = :fono2")
    , @NamedQuery(name = "TRepresentante.findByDireccion", query = "SELECT t FROM TRepresentante t WHERE t.direccion = :direccion")
    , @NamedQuery(name = "TRepresentante.findByVigente", query = "SELECT t FROM TRepresentante t WHERE t.vigente = :vigente")})
public class TRepresentante implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDREPRESENTANTE")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_REPRESENTANTE")
    @SequenceGenerator(name = "SEQ_REPRESENTANTE", sequenceName = "SEQ_REPRESENTANTE", allocationSize = 1)
    private BigDecimal idrepresentante;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "RUTREPRESENTANTE")
    private String rutrepresentante;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "ROLREPRESENTANTE")
    private String rolrepresentante;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "APPATERNO")
    private String appaterno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "APMATERNO")
    private String apmaterno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SEXO")
    private Character sexo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHANACIMIENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechanacimiento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "FONO1")
    private String fono1;
    @Size(max = 15)
    @Column(name = "FONO2")
    private String fono2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "DIRECCION")
    private String direccion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VIGENTE")
    private Character vigente;
    @JoinColumn(name = "IDEMPRESA", referencedColumnName = "IDEMPRESA")
    @ManyToOne(optional = false)
    private TEmpresa idempresa;

    public TRepresentante() {
    }

    public TRepresentante(BigDecimal idrepresentante) {
        this.idrepresentante = idrepresentante;
    }

    public TRepresentante(BigDecimal idrepresentante, String rutrepresentante, String rolrepresentante, String nombre, String appaterno, String apmaterno, Character sexo, Date fechanacimiento, String fono1, String direccion ,Character vigente) {
        this.idrepresentante = idrepresentante;
        this.rutrepresentante = rutrepresentante;
        this.rolrepresentante = rolrepresentante;
        this.nombre = nombre;
        this.appaterno = appaterno;
        this.apmaterno = apmaterno;
        this.sexo = sexo;
        this.fechanacimiento = fechanacimiento;
        this.fono1 = fono1;
        this.direccion = direccion;
        this.vigente = vigente;
    }

    public BigDecimal getIdrepresentante() {
        return idrepresentante;
    }

    public void setIdrepresentante(BigDecimal idrepresentante) {
        this.idrepresentante = idrepresentante;
    }

    public String getRutrepresentante() {
        return rutrepresentante;
    }

    public void setRutrepresentante(String rutrepresentante) {
        this.rutrepresentante = rutrepresentante;
    }

    public String getRolrepresentante() {
        return rolrepresentante;
    }

    public void setRolrepresentante(String rolrepresentante) {
        this.rolrepresentante = rolrepresentante;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAppaterno() {
        return appaterno;
    }

    public void setAppaterno(String appaterno) {
        this.appaterno = appaterno;
    }

    public String getApmaterno() {
        return apmaterno;
    }

    public void setApmaterno(String apmaterno) {
        this.apmaterno = apmaterno;
    }

    public Character getSexo() {
        return sexo;
    }

    public void setSexo(Character sexo) {
        this.sexo = sexo;
    }

    public Date getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(Date fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public String getFono1() {
        return fono1;
    }

    public void setFono1(String fono1) {
        this.fono1 = fono1;
    }

    public String getFono2() {
        return fono2;
    }

    public void setFono2(String fono2) {
        this.fono2 = fono2;
    }

    public Character getVigente() {
        return vigente;
    }

    public void setVigente(Character vigente) {
        this.vigente = vigente;
    }

    public TEmpresa getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(TEmpresa idempresa) {
        this.idempresa = idempresa;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idrepresentante != null ? idrepresentante.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TRepresentante)) {
            return false;
        }
        TRepresentante other = (TRepresentante) object;
        if ((this.idrepresentante == null && other.idrepresentante != null) || (this.idrepresentante != null && !this.idrepresentante.equals(other.idrepresentante))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.maipogrande.entities.TRepresentante[ idrepresentante=" + idrepresentante + " ]";
    }

}
