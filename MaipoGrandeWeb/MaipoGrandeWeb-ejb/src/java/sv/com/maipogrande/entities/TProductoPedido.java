/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rotzedust
 */
@Entity
@Table(name = "T_PRODUCTO_PEDIDO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TProductoPedido.findAll", query = "SELECT t FROM TProductoPedido t")
    , @NamedQuery(name = "TProductoPedido.findByIdproductopedido", query = "SELECT t FROM TProductoPedido t WHERE t.idproductopedido = :idproductopedido")
    , @NamedQuery(name = "TProductoPedido.findByCantidad", query = "SELECT t FROM TProductoPedido t WHERE t.cantidad = :cantidad")})
public class TProductoPedido implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDPRODUCTOPEDIDO")
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "SEQ_PRODUCTO_PEDIDO")
    @SequenceGenerator(name="SEQ_PRODUCTO_PEDIDO",sequenceName = "SEQ_PRODUCTO_PEDIDO",allocationSize = 1)
    private BigDecimal idproductopedido;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CANTIDAD")
    private BigInteger cantidad;
    @JoinColumn(name = "IDPEDIDO", referencedColumnName = "IDPEDIDO")
    @ManyToOne(optional = false,fetch = FetchType.LAZY)
    private TPedido idpedido;
    @JoinColumn(name = "IDPRODUCTO", referencedColumnName = "IDPRODUCTO")
    @ManyToOne(optional = false)
    private TProducto idproducto;
    @JoinColumn(name = "IDUNIDAD", referencedColumnName = "IDUNIDAD")
    @ManyToOne(optional = false)
    private TUnidadMedicion idunidad;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idproductopedido")
    private List<TLoteOfrecido> tLoteOfrecidoList;

    public TProductoPedido() {
    }

    public TProductoPedido(BigDecimal idproductopedido) {
        this.idproductopedido = idproductopedido;
    }

    public TProductoPedido(BigDecimal idproductopedido, BigInteger cantidad) {
        this.idproductopedido = idproductopedido;
        this.cantidad = cantidad;
    }

    public BigDecimal getIdproductopedido() {
        return idproductopedido;
    }

    public void setIdproductopedido(BigDecimal idproductopedido) {
        this.idproductopedido = idproductopedido;
    }

    public BigInteger getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigInteger cantidad) {
        this.cantidad = cantidad;
    }

    public TPedido getIdpedido() {
        return idpedido;
    }

    public void setIdpedido(TPedido idpedido) {
        this.idpedido = idpedido;
    }

    public TProducto getIdproducto() {
        return idproducto;
    }

    public void setIdproducto(TProducto idproducto) {
        this.idproducto = idproducto;
    }

    public TUnidadMedicion getIdunidad() {
        return idunidad;
    }

    public void setIdunidad(TUnidadMedicion idunidad) {
        this.idunidad = idunidad;
    }

    @XmlTransient
    public List<TLoteOfrecido> getTLoteOfrecidoList() {
        return tLoteOfrecidoList;
    }

    public void setTLoteOfrecidoList(List<TLoteOfrecido> tLoteOfrecidoList) {
        this.tLoteOfrecidoList = tLoteOfrecidoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idproductopedido != null ? idproductopedido.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TProductoPedido)) {
            return false;
        }
        TProductoPedido other = (TProductoPedido) object;
        if ((this.idproductopedido == null && other.idproductopedido != null) || (this.idproductopedido != null && !this.idproductopedido.equals(other.idproductopedido))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.maipogrande.entities.TProductoPedido[ idproductopedido=" + idproductopedido + " ]";
    }
    
}
