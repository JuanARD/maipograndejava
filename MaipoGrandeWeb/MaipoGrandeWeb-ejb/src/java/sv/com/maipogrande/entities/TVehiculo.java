/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rotzedust
 */
@Entity
@Table(name = "T_VEHICULO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TVehiculo.findAll", query = "SELECT t FROM TVehiculo t")
    , @NamedQuery(name = "TVehiculo.findByIdvehiculo", query = "SELECT t FROM TVehiculo t WHERE t.idvehiculo = :idvehiculo")
    , @NamedQuery(name = "TVehiculo.findByPatente", query = "SELECT t FROM TVehiculo t WHERE t.patente = :patente")
    , @NamedQuery(name = "TVehiculo.findByClase", query = "SELECT t FROM TVehiculo t WHERE t.clase = :clase")
    , @NamedQuery(name = "TVehiculo.findByCapacidadkg", query = "SELECT t FROM TVehiculo t WHERE t.capacidadkg = :capacidadkg")
    , @NamedQuery(name = "TVehiculo.findByCmancho", query = "SELECT t FROM TVehiculo t WHERE t.cmancho = :cmancho")
    , @NamedQuery(name = "TVehiculo.findByCmalto", query = "SELECT t FROM TVehiculo t WHERE t.cmalto = :cmalto")
    , @NamedQuery(name = "TVehiculo.findByCmlargo", query = "SELECT t FROM TVehiculo t WHERE t.cmlargo = :cmlargo")
    , @NamedQuery(name = "TVehiculo.findByFrigorifico", query = "SELECT t FROM TVehiculo t WHERE t.frigorifico = :frigorifico")})
public class TVehiculo implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDVEHICULO")
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "SEQ_VEHICULO")
    @SequenceGenerator(name="SEQ_VEHICULO",sequenceName = "SEQ_VEHICULO",allocationSize = 1)
    private BigDecimal idvehiculo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "PATENTE")
    private String patente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "CLASE")
    private String clase;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CAPACIDADKG")
    private BigInteger capacidadkg;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CMANCHO")
    private BigInteger cmancho;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CMALTO")
    private BigInteger cmalto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CMLARGO")
    private BigInteger cmlargo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FRIGORIFICO")
    private Character frigorifico;
    @JoinColumn(name = "IDEMPRESA", referencedColumnName = "IDEMPRESA")
    @ManyToOne(optional = false)
    private TEmpresa idempresa;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idvehiculo")
    private List<TOfertaTransporte> tOfertaTransporteList;

    public TVehiculo() {
    }

    public TVehiculo(BigDecimal idvehiculo) {
        this.idvehiculo = idvehiculo;
    }

    public TVehiculo(BigDecimal idvehiculo, String patente, String clase, BigInteger capacidadkg, BigInteger cmancho, BigInteger cmalto, BigInteger cmlargo, Character frigorifico) {
        this.idvehiculo = idvehiculo;
        this.patente = patente;
        this.clase = clase;
        this.capacidadkg = capacidadkg;
        this.cmancho = cmancho;
        this.cmalto = cmalto;
        this.cmlargo = cmlargo;
        this.frigorifico = frigorifico;
    }

    public BigDecimal getIdvehiculo() {
        return idvehiculo;
    }

    public void setIdvehiculo(BigDecimal idvehiculo) {
        this.idvehiculo = idvehiculo;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public BigInteger getCapacidadkg() {
        return capacidadkg;
    }

    public void setCapacidadkg(BigInteger capacidadkg) {
        this.capacidadkg = capacidadkg;
    }

    public BigInteger getCmancho() {
        return cmancho;
    }

    public void setCmancho(BigInteger cmancho) {
        this.cmancho = cmancho;
    }

    public BigInteger getCmalto() {
        return cmalto;
    }

    public void setCmalto(BigInteger cmalto) {
        this.cmalto = cmalto;
    }

    public BigInteger getCmlargo() {
        return cmlargo;
    }

    public void setCmlargo(BigInteger cmlargo) {
        this.cmlargo = cmlargo;
    }

    public Character getFrigorifico() {
        return frigorifico;
    }

    public void setFrigorifico(Character frigorifico) {
        this.frigorifico = frigorifico;
    }

    public TEmpresa getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(TEmpresa idempresa) {
        this.idempresa = idempresa;
    }

    @XmlTransient
    public List<TOfertaTransporte> getTOfertaTransporteList() {
        return tOfertaTransporteList;
    }

    public void setTOfertaTransporteList(List<TOfertaTransporte> tOfertaTransporteList) {
        this.tOfertaTransporteList = tOfertaTransporteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idvehiculo != null ? idvehiculo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TVehiculo)) {
            return false;
        }
        TVehiculo other = (TVehiculo) object;
        if ((this.idvehiculo == null && other.idvehiculo != null) || (this.idvehiculo != null && !this.idvehiculo.equals(other.idvehiculo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.maipogrande.entities.TVehiculo[ idvehiculo=" + idvehiculo + " ]";
    }
    
}
