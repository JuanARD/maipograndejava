/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rotzedust
 */
@Entity
@Table(name = "T_PRODUCTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TProducto.findAll", query = "SELECT t FROM TProducto t")
    , @NamedQuery(name = "TProducto.findByIdproducto", query = "SELECT t FROM TProducto t WHERE t.idproducto = :idproducto")
    , @NamedQuery(name = "TProducto.findByNombre", query = "SELECT t FROM TProducto t WHERE t.nombre = :nombre")
    , @NamedQuery(name = "TProducto.findByTipo", query = "SELECT t FROM TProducto t WHERE t.tipo = :tipo")
//    , @NamedQuery(name = "TProducto.findByNombresFrutas", query = "select DISTINCT(pr.nombre) FROM t_producto pr order by pr.nombre;")
})
public class TProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDPRODUCTO")
    private BigDecimal idproducto;
    @Size(max = 20)
    @Column(name = "NOMBRE")
    private String nombre;
    @Size(max = 30)
    @Column(name = "TIPO")
    private String tipo;
    @Lob
    @Column(name = "FOTO")
    private Serializable foto;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idproducto")
    private List<TProductoPedido> tProductoPedidoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idproducto")
    private List<TLoteProducto> tLoteProductoList;

    public TProducto() {
    }

    public TProducto(BigDecimal idproducto) {
        this.idproducto = idproducto;
    }

    public BigDecimal getIdproducto() {
        return idproducto;
    }

    public void setIdproducto(BigDecimal idproducto) {
        this.idproducto = idproducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Serializable getFoto() {
        return foto;
    }

    public void setFoto(Serializable foto) {
        this.foto = foto;
    }

    @XmlTransient
    public List<TProductoPedido> getTProductoPedidoList() {
        return tProductoPedidoList;
    }

    public void setTProductoPedidoList(List<TProductoPedido> tProductoPedidoList) {
        this.tProductoPedidoList = tProductoPedidoList;
    }

    @XmlTransient
    public List<TLoteProducto> getTLoteProductoList() {
        return tLoteProductoList;
    }

    public void setTLoteProductoList(List<TLoteProducto> tLoteProductoList) {
        this.tLoteProductoList = tLoteProductoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idproducto != null ? idproducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TProducto)) {
            return false;
        }
        TProducto other = (TProducto) object;
        if ((this.idproducto == null && other.idproducto != null) || (this.idproducto != null && !this.idproducto.equals(other.idproducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.maipogrande.entities.TProducto[ idproducto=" + idproducto + " ]";
    }
    
}
