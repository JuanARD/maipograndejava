/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 56971
 */
@Entity
@Table(name = "V_VIAJESMESES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VViajesmeses.findAll", query = "SELECT v FROM VViajesmeses v")
    , @NamedQuery(name = "VViajesmeses.findByIdEmpresa", query = "SELECT v FROM VViajesmeses v WHERE v.idEmpresa = :idEmpresa")
    , @NamedQuery(name = "VViajesmeses.findByEmpresa", query = "SELECT v FROM VViajesmeses v WHERE v.empresa = :empresa")
    , @NamedQuery(name = "VViajesmeses.findByIdRepr", query = "SELECT v FROM VViajesmeses v WHERE v.idRepr = :idRepr")
    , @NamedQuery(name = "VViajesmeses.findByNombreRepresentante", query = "SELECT v FROM VViajesmeses v WHERE v.nombreRepresentante = :nombreRepresentante")
    , @NamedQuery(name = "VViajesmeses.findByOfertaVigente", query = "SELECT v FROM VViajesmeses v WHERE v.ofertaVigente = :ofertaVigente")
    , @NamedQuery(name = "VViajesmeses.findByIdOferta", query = "SELECT v FROM VViajesmeses v WHERE v.idOferta = :idOferta")
    , @NamedQuery(name = "VViajesmeses.findBySubasta", query = "SELECT v FROM VViajesmeses v WHERE v.subasta = :subasta")
    , @NamedQuery(name = "VViajesmeses.findByIdPedido", query = "SELECT v FROM VViajesmeses v WHERE v.idPedido = :idPedido")
    , @NamedQuery(name = "VViajesmeses.findByFechaEntrega", query = "SELECT v FROM VViajesmeses v WHERE v.fechaEntrega = :fechaEntrega")
    , @NamedQuery(name = "VViajesmeses.findByEnBodega", query = "SELECT v FROM VViajesmeses v WHERE v.enBodega = :enBodega")
    , @NamedQuery(name = "VViajesmeses.findByEnRecorrido", query = "SELECT v FROM VViajesmeses v WHERE v.enRecorrido = :enRecorrido")
    , @NamedQuery(name = "VViajesmeses.findByEnDestino", query = "SELECT v FROM VViajesmeses v WHERE v.enDestino = :enDestino")})
public class VViajesmeses implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "ID_EMPRESA")
    private BigInteger idEmpresa;
    @Size(max = 50)
    @Column(name = "EMPRESA")
    private String empresa;
    @Column(name = "ID_REPR")
    private BigInteger idRepr;
    @Size(max = 122)
    @Column(name = "NOMBRE_REPRESENTANTE")
    private String nombreRepresentante;
    @Column(name = "OFERTA_VIGENTE")
    private Character ofertaVigente;
    @Column(name = "ID_OFERTA")
    private BigInteger idOferta;
    @Column(name = "SUBASTA")
    private BigInteger subasta;
    @Column(name = "ID_PEDIDO")
    private BigInteger idPedido;
    @Column(name = "FECHA_ENTREGA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEntrega;
    @Column(name = "En_Bodega")
    private BigInteger enBodega;
    @Column(name = "En_Recorrido")
    private BigInteger enRecorrido;
    @Column(name = "En_Destino")
    private BigInteger enDestino;
    @Id
    private Long id;

    public VViajesmeses() {
    }

    public BigInteger getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(BigInteger idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public BigInteger getIdRepr() {
        return idRepr;
    }

    public void setIdRepr(BigInteger idRepr) {
        this.idRepr = idRepr;
    }

    public String getNombreRepresentante() {
        return nombreRepresentante;
    }

    public void setNombreRepresentante(String nombreRepresentante) {
        this.nombreRepresentante = nombreRepresentante;
    }

    public Character getOfertaVigente() {
        return ofertaVigente;
    }

    public void setOfertaVigente(Character ofertaVigente) {
        this.ofertaVigente = ofertaVigente;
    }

    public BigInteger getIdOferta() {
        return idOferta;
    }

    public void setIdOferta(BigInteger idOferta) {
        this.idOferta = idOferta;
    }

    public BigInteger getSubasta() {
        return subasta;
    }

    public void setSubasta(BigInteger subasta) {
        this.subasta = subasta;
    }

    public BigInteger getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(BigInteger idPedido) {
        this.idPedido = idPedido;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public BigInteger getEnBodega() {
        return enBodega;
    }

    public void setEnBodega(BigInteger enBodega) {
        this.enBodega = enBodega;
    }

    public BigInteger getEnRecorrido() {
        return enRecorrido;
    }

    public void setEnRecorrido(BigInteger enRecorrido) {
        this.enRecorrido = enRecorrido;
    }

    public BigInteger getEnDestino() {
        return enDestino;
    }

    public void setEnDestino(BigInteger enDestino) {
        this.enDestino = enDestino;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
}
