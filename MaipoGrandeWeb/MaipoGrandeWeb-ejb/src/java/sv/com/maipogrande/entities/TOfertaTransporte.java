/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rotzedust
 */
@Entity
@Table(name = "T_OFERTA_TRANSPORTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TOfertaTransporte.findAll", query = "SELECT t FROM TOfertaTransporte t")
    , @NamedQuery(name = "TOfertaTransporte.findByIdofertatransporte", query = "SELECT t FROM TOfertaTransporte t WHERE t.idofertatransporte = :idofertatransporte")
    , @NamedQuery(name = "TOfertaTransporte.findByCostotransporte", query = "SELECT t FROM TOfertaTransporte t WHERE t.costotransporte = :costotransporte")
    , @NamedQuery(name = "TOfertaTransporte.findByVigente", query = "SELECT t FROM TOfertaTransporte t WHERE t.vigente = :vigente")})
public class TOfertaTransporte implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDOFERTATRANSPORTE")
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "SEQ_OFERTA_TRANSPORTE")
    @SequenceGenerator(name="SEQ_OFERTA_TRANSPORTE",sequenceName = "SEQ_OFERTA_TRANSPORTE",allocationSize = 1)
    private BigDecimal idofertatransporte;   
    @Basic(optional = false)
    @NotNull
    @Column(name = "COSTOTRANSPORTE")
    private BigInteger costotransporte;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VIGENTE")
    private Character vigente;
    @JoinColumn(name = "IDREPRESENTANTE", referencedColumnName = "IDREPRESENTANTE")
    @ManyToOne(optional = false)
    private TRepresentante idrepresentante;
    @JoinColumn(name = "IDSUBASTA", referencedColumnName = "IDSUBASTA")
    @ManyToOne(optional = false)
    private TSubasta idsubasta;
    @JoinColumn(name = "IDVEHICULO", referencedColumnName = "IDVEHICULO")
    @ManyToOne(optional = false)
    private TVehiculo idvehiculo;
   
   
    public TOfertaTransporte() {
    }

    public TOfertaTransporte(BigDecimal idofertatransporte) {
        this.idofertatransporte = idofertatransporte;
    }

    public TOfertaTransporte(BigDecimal idofertatransporte, BigInteger costotransporte, Character vigente) {
        this.idofertatransporte = idofertatransporte;
        this.costotransporte = costotransporte;
        this.vigente = vigente;
    }

    public BigDecimal getIdofertatransporte() {
        return idofertatransporte;
    }

    public void setIdofertatransporte(BigDecimal idofertatransporte) {
        this.idofertatransporte = idofertatransporte;
    }

    public BigInteger getCostotransporte() {
        return costotransporte;
    }

    public void setCostotransporte(BigInteger costotransporte) {
        this.costotransporte = costotransporte;
    }

    public Character getVigente() {
        return vigente;
    }

    public void setVigente(Character vigente) {
        this.vigente = vigente;
    }

    public TRepresentante getIdrepresentante() {
        return idrepresentante;
    }

    public void setIdrepresentante(TRepresentante idrepresentante) {
        this.idrepresentante = idrepresentante;
    }

    public TSubasta getIdsubasta() {
        return idsubasta;
    }

    public void setIdsubasta(TSubasta idsubasta) {
        this.idsubasta = idsubasta;
    }

    public TVehiculo getIdvehiculo() {
        return idvehiculo;
    }

    public void setIdvehiculo(TVehiculo idvehiculo) {
        this.idvehiculo = idvehiculo;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idofertatransporte != null ? idofertatransporte.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TOfertaTransporte)) {
            return false;
        }
        TOfertaTransporte other = (TOfertaTransporte) object;
        if ((this.idofertatransporte == null && other.idofertatransporte != null) || (this.idofertatransporte != null && !this.idofertatransporte.equals(other.idofertatransporte))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.maipogrande.entities.TOfertaTransporte[ idofertatransporte=" + idofertatransporte + " ]";
    }
    
}
