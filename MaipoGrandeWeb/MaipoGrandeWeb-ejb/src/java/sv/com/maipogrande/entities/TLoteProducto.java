/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rotzedust
 */
@Entity
@Table(name = "T_LOTE_PRODUCTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TLoteProducto.findAll", query = "SELECT t FROM TLoteProducto t")
    , @NamedQuery(name = "TLoteProducto.findByIdloteproducto", query = "SELECT t FROM TLoteProducto t WHERE t.idloteproducto = :idloteproducto")
    , @NamedQuery(name = "TLoteProducto.findByCantidadproductos", query = "SELECT t FROM TLoteProducto t WHERE t.cantidadproductos = :cantidadproductos")
    , @NamedQuery(name = "TLoteProducto.findByPreciounidad", query = "SELECT t FROM TLoteProducto t WHERE t.preciounidad = :preciounidad")
    , @NamedQuery(name = "TLoteProducto.findByCalidad", query = "SELECT t FROM TLoteProducto t WHERE t.calidad = :calidad")
    , @NamedQuery(name = "TLoteProducto.findByFechavencimiento", query = "SELECT t FROM TLoteProducto t WHERE t.fechavencimiento = :fechavencimiento")
    , @NamedQuery(name = "TLoteProducto.findByCondicionesalmacenaje", query = "SELECT t FROM TLoteProducto t WHERE t.condicionesalmacenaje = :condicionesalmacenaje")
    , @NamedQuery(name = "TLoteProducto.findByVigente", query = "SELECT t FROM TLoteProducto t WHERE t.vigente = :vigente")
    , @NamedQuery(name = "TLoteProducto.findByVentaalextranjero", query = "SELECT t FROM TLoteProducto t WHERE t.ventaalextranjero = :ventaalextranjero")})
public class TLoteProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDLOTEPRODUCTO")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_LOTE_PRODUCTO")
    @SequenceGenerator(name = "SEQ_LOTE_PRODUCTO", sequenceName = "SEQ_LOTE_PRODUCTO", allocationSize = 1)
    private BigDecimal idloteproducto;
    @Column(name = "CANTIDADPRODUCTOS")
    private BigInteger cantidadproductos;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRECIOUNIDAD")
    private BigInteger preciounidad;
    @Column(name = "CALIDAD")
    private BigInteger calidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHAVENCIMIENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechavencimiento;
    @Size(max = 300)
    @Column(name = "CONDICIONESALMACENAJE")
    private String condicionesalmacenaje;
    @Lob
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VIGENTE")
    private Character vigente;
    @Column(name = "VENTAALEXTRANJERO")
    private Character ventaalextranjero;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idloteproducto")
    private List<TLoteOfrecido> tLoteOfrecidoList;
    @JoinColumn(name = "IDEMPRESA", referencedColumnName = "IDEMPRESA")
    @ManyToOne(optional = false)
    private TEmpresa idempresa;
    @JoinColumn(name = "IDPRODUCTO", referencedColumnName = "IDPRODUCTO")
    @ManyToOne(optional = false)
    private TProducto idproducto;
    @JoinColumn(name = "IDUNIDAD", referencedColumnName = "IDUNIDAD")
    @ManyToOne(optional = false)
    private TUnidadMedicion idunidad;

    public TLoteProducto() {
        init();
    }
    @PostConstruct
    public void init(){
        this.condicionesalmacenaje = "1";
        this.ventaalextranjero = '1';
    }

    public TLoteProducto(BigDecimal idloteproducto) {
        this.idloteproducto = idloteproducto;
    }

    public TLoteProducto(BigDecimal idloteproducto, BigInteger preciounidad, Date fechavencimiento, Character vigente) {
        this.idloteproducto = idloteproducto;
        this.preciounidad = preciounidad;
        this.fechavencimiento = fechavencimiento;
        this.vigente = vigente;
    }

    public BigDecimal getIdloteproducto() {
        return idloteproducto;
    }

    public void setIdloteproducto(BigDecimal idloteproducto) {
        this.idloteproducto = idloteproducto;
    }

    public BigInteger getCantidadproductos() {
        return cantidadproductos;
    }

    public void setCantidadproductos(BigInteger cantidadproductos) {
        this.cantidadproductos = cantidadproductos;
    }

    public BigInteger getPreciounidad() {
        return preciounidad;
    }

    public void setPreciounidad(BigInteger preciounidad) {
        this.preciounidad = preciounidad;
    }

    public BigInteger getCalidad() {
        return calidad;
    }

    public void setCalidad(BigInteger calidad) {
        this.calidad = calidad;
    }

    public Date getFechavencimiento() {
        return fechavencimiento;
    }

    public void setFechavencimiento(Date fechavencimiento) {
        this.fechavencimiento = fechavencimiento;
    }

    public String getCondicionesalmacenaje() {
        return condicionesalmacenaje;
    }

    public void setCondicionesalmacenaje(String condicionesalmacenaje) {
        this.condicionesalmacenaje = condicionesalmacenaje;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Character getVigente() {
        return vigente;
    }

    public void setVigente(Character vigente) {
        this.vigente = vigente;
    }

    public Character getVentaalextranjero() {
        return ventaalextranjero;
    }

    public void setVentaalextranjero(Character ventaalextranjero) {
        this.ventaalextranjero = ventaalextranjero;
    }

    @XmlTransient
    public List<TLoteOfrecido> getTLoteOfrecidoList() {
        return tLoteOfrecidoList;
    }

    public void setTLoteOfrecidoList(List<TLoteOfrecido> tLoteOfrecidoList) {
        this.tLoteOfrecidoList = tLoteOfrecidoList;
    }

    public TEmpresa getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(TEmpresa idempresa) {
        this.idempresa = idempresa;
    }

    public TProducto getIdproducto() {
        return idproducto;
    }

    public void setIdproducto(TProducto idproducto) {
        this.idproducto = idproducto;
    }

    public TUnidadMedicion getIdunidad() {
        return idunidad;
    }

    public void setIdunidad(TUnidadMedicion idunidad) {
        this.idunidad = idunidad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idloteproducto != null ? idloteproducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TLoteProducto)) {
            return false;
        }
        TLoteProducto other = (TLoteProducto) object;
        if ((this.idloteproducto == null && other.idloteproducto != null) || (this.idloteproducto != null && !this.idloteproducto.equals(other.idloteproducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.maipogrande.entities.TLoteProducto[ idloteproducto=" + idloteproducto + " ]";
    }
    
}
