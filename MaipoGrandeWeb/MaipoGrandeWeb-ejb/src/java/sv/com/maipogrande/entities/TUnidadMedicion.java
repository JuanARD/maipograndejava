/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rotzedust
 */
@Entity
@Table(name = "T_UNIDAD_MEDICION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TUnidadMedicion.findAll", query = "SELECT t FROM TUnidadMedicion t")
    , @NamedQuery(name = "TUnidadMedicion.findByIdunidad", query = "SELECT t FROM TUnidadMedicion t WHERE t.idunidad = :idunidad")
    , @NamedQuery(name = "TUnidadMedicion.findByDescripcion", query = "SELECT t FROM TUnidadMedicion t WHERE t.descripcion = :descripcion")})
public class TUnidadMedicion implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDUNIDAD")
    private BigDecimal idunidad;
    @Size(max = 20)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idunidad")
    private List<TProductoPedido> tProductoPedidoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idunidad")
    private List<TLoteProducto> tLoteProductoList;

    public TUnidadMedicion() {
    }

    public TUnidadMedicion(BigDecimal idunidad) {
        this.idunidad = idunidad;
    }

    public BigDecimal getIdunidad() {
        return idunidad;
    }

    public void setIdunidad(BigDecimal idunidad) {
        this.idunidad = idunidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<TProductoPedido> getTProductoPedidoList() {
        return tProductoPedidoList;
    }

    public void setTProductoPedidoList(List<TProductoPedido> tProductoPedidoList) {
        this.tProductoPedidoList = tProductoPedidoList;
    }

    @XmlTransient
    public List<TLoteProducto> getTLoteProductoList() {
        return tLoteProductoList;
    }

    public void setTLoteProductoList(List<TLoteProducto> tLoteProductoList) {
        this.tLoteProductoList = tLoteProductoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idunidad != null ? idunidad.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TUnidadMedicion)) {
            return false;
        }
        TUnidadMedicion other = (TUnidadMedicion) object;
        if ((this.idunidad == null && other.idunidad != null) || (this.idunidad != null && !this.idunidad.equals(other.idunidad))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.maipogrande.entities.TUnidadMedicion[ idunidad=" + idunidad + " ]";
    }
    
}
