package sv.com.maipogrande.authent;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import javax.ejb.Stateless;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

@Stateless
public class AutenticarBean implements AutentificacionRemota {
    @PersistenceContext(unitName = "MaipoGrandeWeb-ejbPU")
    EntityManager em;
    protected EntityManager getEntityManager() {
        return em;
    }

    public boolean autenticar(String email, String pass) {

            String correo_salida, clave_salida = "";

            // llamada procedure
            StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("ValidarUsuario");
            // set parameters
            storedProcedure.registerStoredProcedureParameter("V_CORREO", String.class, ParameterMode.IN);
            storedProcedure.registerStoredProcedureParameter("V_CLAVE", String.class, ParameterMode.IN);
            storedProcedure.registerStoredProcedureParameter("P_EMAIL", String.class, ParameterMode.OUT);
            storedProcedure.registerStoredProcedureParameter("P_CONTRASENA", String.class, ParameterMode.OUT);

            storedProcedure.setParameter("V_CORREO", email);
            storedProcedure.setParameter("V_CLAVE", pass);
            // execute procedure
            
            storedProcedure.execute();

            // get resultados
            correo_salida = (String) storedProcedure.getOutputParameterValue("P_EMAIL");
            clave_salida = (String) storedProcedure.getOutputParameterValue("P_CONTRASENA");
            System.out.println("clave" + clave_salida);

            if (correo_salida == "" || clave_salida == "") {
                clave_salida = "error:xe";
                correo_salida = "error:xe";
            }
            System.out.println("Respuesta=" + correo_salida);
            System.out.println("Respuesta=" + clave_salida);

        return correo_salida.equals(email) && clave_salida.equals(pass);
        
    }
}
