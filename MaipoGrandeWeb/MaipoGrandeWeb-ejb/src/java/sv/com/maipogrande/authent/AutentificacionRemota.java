/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.maipogrande.authent;

import javax.ejb.Remote;

/**
 *
 * @author Maxi
 */
@Remote
public interface AutentificacionRemota {
    public boolean autenticar(String email, String pass);
}
